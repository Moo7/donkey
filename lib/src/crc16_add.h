/** 
 * Create cyclic redundancy checks using CRC-CCIT (16BIT)
 * algorithm.
 *
 * REMARK:
 * Derived from crc16_ccit (http://www8.cs.umu.se/~isak/snippets/crc-16.c)
 */
unsigned short crc16_add(char* p, unsigned short length, unsigned short crc);

