#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "donkey_frame.h"
#include "donkey.h"

static const char* donkey_fid_syn = DONKEY_FID_SYN;
static const char* donkey_fid_ack = DONKEY_FID_ACK;
static const char* donkey_fid_stx = DONKEY_FID_STX;
static const char* donkey_fid_nak = DONKEY_FID_NAK;
static const char* donkey_fid_bin = DONKEY_FID_BIN;
static const char* donkey_fid_rst = DONKEY_FID_RST;

static char* donkey_fid[] = {
	DONKEY_FID_SYN,
	DONKEY_FID_ACK,
	DONKEY_FID_STX,
	DONKEY_FID_NAK,
	DONKEY_FID_BIN,
	DONKEY_FID_RST,
	NULL
};

/**
 * Adds a cyclic redundancy check to the given frame
 *
 * @param	frame	frame instance
 * @param	len		length in bytes of the data section
 * @return			void
 */
static void donkey_frame_add_crc(uint8_t* frame, uint16_t len)
{
	uint16_t crc;
	size_t size = DONKEY_FRAME_HEADER + len;

	crc = crc16_ccitt(frame, size);
	crc = htons(crc);
	memcpy(frame + size, &crc, DONKEY_CRC);
}

/**
 * Encodes a frame using the given frame arguments and payload.
 *
 * @param	frame	frame instance
 * @param	fid		frame identifier
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @param	data	frame payload
 * @return			void
 */
static size_t donkey_frame_encode(uint8_t* frame, const char* fid, uint16_t sid, uint16_t len, const uint8_t* data)
{
	uint16_t length = htons(len);
	size_t size = DONKEY_FRAME_HEADER + len + DONKEY_CRC;
	uint8_t* dst = frame + DONKEY_O_DATA;

	// init header
	memcpy(frame, fid, DONKEY_FID);
	sid = htons(sid);
	memcpy(frame + DONKEY_O_SID, &sid, DONKEY_SID);
	frame[DONKEY_O_TOK] = '@';
	frame[DONKEY_O_TOK + 1] = '$';
	memcpy(frame + DONKEY_O_LEN, &length, DONKEY_LEN);

	// add data
	if (len) {
		if (data) {
			if (data != dst) {
				memcpy(dst, data, len);
			}
		}
		else {
			memset(dst, 0, len);
		}
	}

	// add crc
	donkey_frame_add_crc(frame, len);
	return size;
}

/**
 * Returns the size in bytes of a frame based on the given 
 * data length.
 *
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_size(uint16_t len)
{
	return (DONKEY_FRAME_EMPTY + len);
}

/**
 * Encodes a synchronization frame (SYN)
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_syn(uint8_t* frame, uint16_t sid)
{
	return donkey_frame_encode(frame, donkey_fid_syn, sid, 0, NULL);
}

/**
 * Encodes a start transmission frame (STX)
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_stx(uint8_t* frame, uint16_t sid, uint16_t len, uint8_t* data)
{
	return donkey_frame_encode(frame, donkey_fid_stx, sid, len, data);
}

/**
 * Encodes a acknowledge frame (ACK) for a SYN frame
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_ack_syn(uint8_t* frame, uint16_t sid)
{
	return donkey_frame_encode(frame, donkey_fid_ack, sid, DONKEY_FID, donkey_fid_syn);
}

/**
 * Encodes a acknowledge frame (ACK) for a STX frame
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_ack_stx(uint8_t* frame, uint16_t sid)
{
	return donkey_frame_encode(frame, donkey_fid_ack, sid, DONKEY_FID, donkey_fid_stx);
}

/**
 * Encodes a not acknowledged frame (NAK)
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_nak(uint8_t* frame, uint16_t sid)
{
	return donkey_frame_encode(frame, donkey_fid_nak, sid, 0, NULL);
}

/**
 * Encodes a binary data frame (BIN)
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_bin(uint8_t* frame, uint16_t sid, uint16_t len, uint8_t* data)
{
	return donkey_frame_encode(frame, donkey_fid_bin, sid, len, data);
}

/**
 * Encodes a link reset frame (RST)
 *
 * @param	frame	frame instance
 * @param	sid		session identifier
 * @param	len		length in bytes of the data section
 * @return			the size in bytes of the encode frame
 */
size_t donkey_frame_rst(uint8_t* frame, uint16_t sid)
{
	return donkey_frame_encode(frame, donkey_fid_rst, sid, 0, NULL);
}

/**
 * Returns lenght in bytes of the data section of a frame
 *
 * @param	frame	frame instance
 * @return			see description
 */
uint16_t donkey_frame_get_len(uint8_t* frame)
{
	uint16_t len;
	memcpy(&len, frame + DONKEY_O_LEN, DONKEY_LEN);
	return htons(len);
}

/**
 * Return pointer to the data section of a frame
 *
 * @param	frame	frame instance
 * @return			see description
 */
uint8_t* donkey_frame_get_data(uint8_t* frame)
{
    return (frame + DONKEY_O_DATA);
}

/**
 * Returns true when the given frame starts with a valid header
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_is_frame_header(uint8_t* frame)
{
	char f[DONKEY_FID + 1];
	char* fid;

	if (frame[0] != '#') {
		return false;
	}

	memcpy(f, frame, DONKEY_FID);
	f[DONKEY_FID] = 0;

	for (fid=donkey_fid[0]; fid; fid++) {
		if (0 == strcmp(f, fid)) {
			break;
		}
	}
	if ( !fid ) {
		return false;
	}

	if ( (frame[DONKEY_O_TOK] != '@') || (frame[DONKEY_O_TOK + 1] != '$') ) {
		return false;
	}
	return true;
}

/**
 * Returns true when the given frame is valid (e.g. valid FID, CRC, LEN, ..)
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_is_frame(uint8_t* frame)
{
	uint16_t crc1, crc2, len;
	size_t size;

	if ( !donkey_is_frame_header(frame) ) {
		return false;
	}

	len = donkey_frame_get_len(frame);
	size = DONKEY_FRAME_HEADER + len;

	crc1 = crc16_ccitt(frame, size);
	memcpy(&crc2, (frame + size), sizeof(crc2));
	crc2 = ntohs(crc2);
	return (crc1 == crc2);
}

/**
 * Returns true when this is a SYN frame.
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_syn(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_syn, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a STX frame.
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_stx(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_stx, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a ACK frame.
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_ack(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_ack, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a ACK frame in response to a SYN frame,
 * i.e. the data contains a #SYN frame identifier.
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_ack_syn(uint8_t* frame)
{
	uint16_t len;

	if (!donkey_frame_is_ack(frame)) {
		return false;
	}

	len = donkey_frame_get_len(frame);
	if (len != DONKEY_FID) {
		return false;
	}
	return (memcmp(frame + DONKEY_O_DATA, donkey_fid_syn, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a ACK frame in response to a STX frame,
 * i.e. the data contains a #STX frame identifier.
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_ack_stx(uint8_t* frame)
{
	uint16_t len;

	if (!donkey_frame_is_ack(frame)) {
		return false;
	}

	len = donkey_frame_get_len(frame);
	if (len != DONKEY_FID) {
		return false;
	}
	return (memcmp(frame + DONKEY_O_DATA, donkey_fid_stx, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a NAK frame
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_nak(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_nak, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a BIN frame
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_bin(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_bin, DONKEY_FID) == 0);
}

/**
 * Returns true when this is a RST frame
 *
 * @param	frame	frame instance
 * @return			see description
 */
bool donkey_frame_is_rst(uint8_t* frame)
{
	return (memcmp(frame, donkey_fid_rst, DONKEY_FID) == 0);
}

