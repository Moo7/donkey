Summary
=======
Donkey is a serial communication protocol for (soft) real time embedded
systems providing deterministic, i.e. will be complete and in time, as 
well as effort data transfer, i.e. will be complete as soon as possible.


Description
===========
Donkey divides the available bandwidth of a serial connection into several 
different donkey trails (data channels) that are either deterministic but 
limited in size or are undeterministic; i.e. the arrival time of the data 
to be transferred is uncertain, but providing (a theoretical) unlimited size.

Data can be transferred using one of the following trails: 


````
    -------------------------------------------------------
    | Name        | Type                   | Period (ms)  |
    -------------------------------------------------------
    | MMAP_1S     | deterministic          |        1000  |
    -------------------------------------------------------
    | MMAP_100MS  | deterministic          |         100  |
    -------------------------------------------------------
    | MMAP_10MS   | deterministic          |          10  |
    -------------------------------------------------------
    | MMAP_1MS    | deterministic          |           1  |
    -------------------------------------------------------
    | MMAP        | best effort            |        n.a.  |
    -------------------------------------------------------
````

Note that some of periodic data transfers type may not be supported, or may not 
be efficient on certain systems due to bandwidth limitations on the physical 
link layer.


Deterministic data transfer 
---------------------------
When using periodic data transfer (1S/100MS/10MS/1MS) data will arrive within
or up to the maximum pediod time that has been specified for the connection. The 
size of the data used for periodic transfers must be specified during system 
start and will be verified against the maximum available bandwidth (e.g. 256 Bytes 
at a periodic interval of 100ms). Furthermore allocating a size for periodic data
transfer will reduce the remaining available bandwidth for 'normal' data transfers
as well as periodic transfer having a lower refresh rate; i.e. having a larger 
period time.


Best effort data transfer
-------------------------
When using ASAP transfer data will be send as soon as possible using the 
available bandwidth provided by the physical link; e.g. using a serial connection 
with a connection speed of 115K2 BAUD. However there is no garantuee as to 'when' 
the data will have arrived. The time needed for the arrival of a block of data 
depends on the available bandwidth and the utilization of that bandwidth.



Protocol description
====================

Frame structure
---------------
Each frame contains a frame- and session identifier and a data payload. The size of
the data is defined by the lenght field in the frame. The correctness of the data
field can be verified using the CRC:

````
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | 4      | 2      | 2      | 2      | N                           | 2      |
    ----------------------------------------------------------------------------

Where:
    FID = frame identifier
    SID = session identifier
    TOK = fixed frame token
    LEN = length in bytes of the data field
    CRC = cyclic redundancy check (CRC-CCITT) of the FID, SID, RES, LEN and DATA fields
````

The frame identifier is used at the link layer only and is not visible at other
layers of the protocol (nor by the user).

The session identifier is a uin16_t identifier. This session identifier is not
static but must be configured and be unique for each system configuration. Both
peers of the connection will use it to verify the system configuration used by
the other peer.


Bandwith and data size
----------------------
When using a serial connection of 115K2 bps the maximum effective bandwith in bytes
per 100ms is:

````

115200 / (10 * 10) = 1152 Bytes

````

When using a serial connection @115K2 the (theoretical) maximum data size that
can be used is:

````

BYTES_PER_100MS - FRAME_OVERHEAD (FID + SID + TOK + LEN + CRC)
1152            -                (4   + 2   + 2   + 2   + 2)   = 1140 Bytes
                                                                 ----------

````


Frame data structure
--------------------
In order to send data at various refresh rates a sub-frame structure is needed
within the data section of each frame:


````
 -------------------------------------------------------------------------------
 | DATA                                                                        |
 -------------------------------------------------------------------------------
 | 100MS SECTION        | 1S SECTION                | SERIALIZED / MMAP        |
 -------------------------------------------------------------------------------
 | IDx | LEN | DATA     | FR | DATA                 |                          |
 -------------------------------------------------------------------------------
 | 2   | 2   | 256      | 2  | 260                  | 584                      | == 1106 bytes
 -------------------------------------------------------------------------------
                            /                       /
                           /                       /
                          /                       /
                         /                       /
    -------------------------               ------------------------------------
    | 2600 per second       |               | max 5800 per second*             | 
    -------------------------               ------------------------------------
    | IDx | LEN | DATA	    |               | IDx  | LENx |                    |
    -------------------------               ------------------------------------
    | 2   | 2   | 0 .. 2596 |               | 2    | 2    | 580                | 
    -------------------------               ------- ------ ---------------------
      \                                            /                /
       \                                          /                /
        \                                        /                /
    ---------------          ---------------------   ---------------------------
    | b15 | b14-0 |          | b15 | b14 | b13-0 |   | SEG  | DATA             |
    ---------------          ---------------------   ---------------------------
    | iB  | ID    |          | iS  | iL  | LEN   |   | 4    | 0 .. 576         |
    ---------------          ---------------------   ---------------------------

	* Maximum transfer rate in bytes per second is 5800 when using a single
	publisher/subscriber using a data size of 580 bytes; this will mmap will
	be refreshed every 100ms.

    Identifiers:
    ID   = Unique publication/subscription identifier each publication identifier
           should match with exactly one subscription identifier on the other
           peer and vice versa.
    FR   = Frame counter ranging from 0 to 9 indicating the current position
           of the 1S memory being transferred.
    LEN  = Length field indicating the size in bytes of the data field.
    DATA = Data being transferred.
    iS   = (bit) Indicates if the data field is segmented in to multiple parts.
           When true the data field will be prepended with the SEG field.
    iL   = (bit) Indicates if this is the last part of a segmented data section.
    iB   = (bit) Indicates if this is a fixed size data block (iB==1) used for
           serialized data transfer or if this is a memory map (iB==0).
    SEG  = Current segment. Maximum file size = 4G * 574

    Ranges:
    LEN   = [0 - 16347]
    ID    = [0 - 32767]
    FR    = [0 - 9]
````

One datablock of 256 bytes will be available in the 100ms section. This section
will be used for transmitting intersection- and lamp states between slave and
master.
A total of 2580 bytes will be available in the 1s section. This allows for upto
ten blocks of 256 bytes inlcuding a two byte identifier and lenght indication.
The frame field will be used for identifying the current 1s fragment being
send [0 .. 9].
The remaining bandwith will  be used to serialize transfer of binary data. When
transmitting large files.

````
    TOTAL FRAME SIZE:
            frame               + data =
            (4 + 2 + 2 + 2 + 2) + 1106 = 1118
````


Frame types
-----------
The following frame types are used by the protocol:

````
    SYN = send by peer while establishing link layer
    STX = send once by peer, completes and establishes link
    ACK = send by peer while establishing link layer
    NAK = send by peer in case of error
    BIN = data frame send once link layer has been established
	RST = terminates current link and start a new one

    Synchronization frame:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #SYN   | xx     | @$     | 0      | none                        | crc16  |
    ----------------------------------------------------------------------------

    REMARK
    will be re-send every 100ms until an ACK(SYN) frame has been reeceived

    Acknowledge frame send once in response to SYN frame:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #ACK   | xx     | @$     | 4      | #SYN                        | crc16  |
    ----------------------------------------------------------------------------

    Start transmit frame send once after ACK(SYN) frame has been received:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #STX   | xx     | @$     | 32     | SESSION-INFO | SESSION-CRC  | crc16  |
    ----------------------------------------------------------------------------
                                             /                /
                                            /                /
    -------------------------------------------------------------------------------------
    | SESSION-INFO                                                                      |
    -------------------------------------------------------------------------------------
    | MMAP 100ms                | MMAP 1s                   | MMAP / SERIAL             |
    -------------------------------------------------------------------------------------
    | PUB         | SUB         | PUB         | SUB         | PUB         | SUB         |
    -------------------------------------------------------------------------------------
    | SIZE | NREL | SIZE | NREL | SIZE | NREL | SIZE | NREL | SIZE | NREL | SIZE | NREL |
    -------------------------------------------------------------------------------------
    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 4    | 2    | 4    | 2    | == 28 bytes
    -------------------------------------------------------------------------------------
                                                /
                                               /
    -------------------------------------------------------------------------------------
    | SESSION-CRC                                                                       |
    -------------------------------------------------------------------------------------
    |     MMAP 100ms            | MMAP 1s                   | MMAP / SERIAL             |
    -------------------------------------------------------------------------------------
    | PUB         | SUB         | PUB         | SUB         | PUB         | SUB         |
    -------------------------------------------------------------------------------------
    | ID   | SIZE | ID   | SIZE | ID   | SIZE | ID   | SIZE | ID   | SIZE | ID   | SIZE |
    -------------------------------------------------------------------------------------
    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | 2    | == 24 bytes
    -------------------------------------------------------------------------------------


    Acknowledge frame send once in response to STX frame:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #ACK   | xx     | @$     | 4      | #STX                        | crc16  |
    ----------------------------------------------------------------------------

    Not acknowledged frame send in case of exceptions:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #NAK   | xx     | @$     | 0      | none                        | crc16  |
    ----------------------------------------------------------------------------

    Data frame send once the link has been established:
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #BIN   | xx     | @$     | yy     | zzzzzzzzzzzzzzzzzzzzzzzzzzz | crc16  |
    ----------------------------------------------------------------------------

    Reset link frame send when forcing a new link session
    ----------------------------------------------------------------------------
    | FID    | SID    | TOK    | LEN    | DATA                        | CRC    |
    ----------------------------------------------------------------------------
    | #RST   | xx     | @$     | 0      | none                        | crc16  |
    ----------------------------------------------------------------------------
````


Establish link
--------------
Before normal data between both peers can be transferred the peers will
synchronize and verify the data (i.e. number, identity and size of each
publisher and subscriber), to be transferred.

Example of a normal connect and data transfer sequence:

````
    PeerX                 PeerY
    -----                 -----
     |                     |
    SYNC                  SYNC
	 | x---- connect ----x |
	 |					   |
     |  ------SYN--------> | ---->|
	 |                     |      |
 |<--| <------SYN--------- |      |
 |   | <------ACK(SYN)---- | <----|
 |	 |                     |
 |-->|  ------ACK(SYN)---> |
	 |  ------STX--------> | ---->|
    START                  |      |
 |<--| <------STX--------- |      |
 |	 | <----- ACK(STX)----START <-|
 |	 |                     |
 |   |                     |
 |-->|  ------ACK(STX)---> |
     |                     |
	ONLINE                ONLINE
	 |                     |
	 | <------DATA-------> |
````


Example of a failure when establishing a link:

````
    PeerX                 PeerY
    -----                 -----
     |                     |
    SYNC                  SYNC
	 | x---- connect ----x |
	 |					   |
     |  ------SYN--------> | ---->|
	 |                     |      |
 |<--| <------SYN--------- |      |
 |   | <------ACK(SYN)---- | <----|
 |	 |                     |
 |-->|  ------ACK(SYN)---> |
	 |  ------STX--------> | ------>| => invalid pub/sub metadata
    START                  |        |
     | <----- NAK--------- |        |
 |<--| <----- SYN---------OFFLINE <-|
 |	 |                     |
 |  OFFLINE                |
 |-->|  ------SYN--------> |
     |                     |
````

Whenever corrupt or illegal frame is received by a peer and the state is online
the current link will be terminated and a new link will be created between both
peers.

Illegal frame received:

````
    PeerX                 PeerY
    -----                 -----
	ONLINE                ONLINE
	 |                     |
	 | -------ILLEGAL----> |
	 |                     |
	 | <------NAK---------SYNC
	SYNC                   |
	 |                     |
````

