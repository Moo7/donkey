#ifndef DONKEY_MMAP_H
#define DONKEY_MMAP_H

#include "config.h"

#if HAVE_STDINT_H
#include <stdint.h>
#endif

#if HAVE_STDBOOL_H
#include <stdbool.h>
#endif

/**
 * Size, data and frame of the current segment (part) of a memory map
 * in a synchronization sequence
 */
typedef struct {
	size_t		size;
	uint8_t*	data;
	uint32_t	segment;
} donkey_sync_t;

/**
 * Contains cyclic redundancy checksums of identifiers and sizes of
 * all mmap users
 */
typedef struct {
	uint16_t	id;
	uint16_t	size;
} donkey_crc_t;

void* donkey_mmap_create(size_t size);
void* donkey_mmap_atomic(size_t size, uint16_t id);
void donkey_mmap_destroy(void* mmap);

void* donkey_mmap_alloc(void* mmap, uint16_t id, size_t size);
size_t donkey_mmap_avail(void* mmap);
size_t donkey_mmap_size(void* mmap);
uint16_t donkey_mmap_nrel(void* mmap);
uint8_t* donkey_mmap_data(void* mmap);
donkey_crc_t donkey_mmap_crc(void* mmap);

donkey_sync_t donkey_mmap_sync(void* mmap, size_t size);
void donkey_mmap_reset(void* mmap);
bool donkey_mmap_is_synced(void* mmap);

#endif // DONKEY_MMAP_H


