#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
#ifdef __linux__
 #include <unistd.h>
#else
 #include <windows.h>
 #define usleep(t) Sleep(t/1000)
#endif
**/

#include "CuTest.h"
#include "donkey.h"

extern donkey_dev_t serdev_open(const char* name);
extern void serdev_close(donkey_dev_t device);

static donkey_dev_t device1;
static donkey_dev_t device2;

bool publish(donkey_type_t type, uint16_t id, size_t size, void* data, void* user)
{
	memset(data, id, size);
	return true;
}

void consume(donkey_type_t type, uint16_t id, size_t size, void* data, void* user)
{
	int i;
	uint8_t* p = (uint8_t*)data;
	CuTest* tc = (CuTest*)user;

	for (i=0; i < size; i++) {
		CuAssert(tc, "consume() p[i] == id", p[i] == id);	
	}
}

void tc1_create_delete(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc2_create_pub_100ms(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 256, publish, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_pub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc3_create_pub_1s(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 2048, publish, tc, DONKEY_T_MMAP_1S);
	CuAssert(tc, "donkey_pub(1,2048,DONKEY_T_MMAP_1S) == 0", res == 0);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc4_create_publications(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 256, publish, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_pub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_publication(donkey, 2, 2048, publish, tc, DONKEY_T_MMAP_1S);
	CuAssert(tc, "donkey_pub(2,2048,DONKEY_T_MMAP_1S) == 0", res == 0);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc5_duplicate_publications(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 256, publish, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_pub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_publication(donkey, 1, 2048, publish, tc, DONKEY_T_MMAP_1S); // FAIL: using duplicate ID
	CuAssert(tc, "donkey_pub(1,2048,DONKEY_T_MMAP_1S) == 0", res != 0);
	CuAssert(tc, "donkey_errno == EINVAL", donkey_errno == EINVAL);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc6_duplicate_subscriptions(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_subscribe(donkey, 1, 256, consume, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_sub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_subscribe(donkey, 1, 2048, consume, tc, DONKEY_T_MMAP_1S); // FAIL: using duplicate ID
	CuAssert(tc, "donkey_sub(1,2048,DONKEY_T_MMAP_1S) == 0", res != 0);
	CuAssert(tc, "donkey_errno == EINVAL", donkey_errno == EINVAL);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc7_pub_100ms_no_mem(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 256, publish, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_pub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_publication(donkey, 2, 256, publish, tc, DONKEY_T_MMAP_100MS); // FAIL: no space left
	CuAssert(tc, "donkey_pub(2,256,DONKEY_T_MMAP_100MS) != 0", res != 0);
	CuAssert(tc, "donkey_errno == ENOMEM", donkey_errno == ENOMEM);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc8_pub_1s_no_mem(CuTest* tc)
{
	int res;
	void* donkey;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 2048, publish, tc, DONKEY_T_MMAP_1S);
	CuAssert(tc, "donkey_pub(1,2048,DONKEY_T_MMAP_1S) == 0", res == 0);

	res = donkey_publication(donkey, 2, 2048, publish, tc, DONKEY_T_MMAP_1S); // FAIL: no space left
	CuAssert(tc, "donkey_pub(2,2048,DONKEY_T_MMAP_1S) != 0", res != 0);
	CuAssert(tc, "donkey_errno == ENOMEM", donkey_errno == ENOMEM);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc9_pub_mmap_info(CuTest* tc)
{
	int res;
	void* donkey;
	donkey_info_t info;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 580, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(1,580,DONKEY_T_MMAP) == 0", res == 0);

	info = donkey_pub_info(donkey, 1);
	CuAssert(tc, "info.t_best == 100", info.t_best == 100);
	CuAssert(tc, "info.bps_best == 5800", info.bps_best == 5800);
	CuAssert(tc, "info.t_worst == info.t_best", info.t_worst == info.t_best);
	CuAssert(tc, "info.bps_worst == info.bps_best", info.bps_worst == info.bps_best);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc10_pub_mmap_info(CuTest* tc)
{
	int res;
	void* donkey;
	donkey_info_t info;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_publication(donkey, 1, 580, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(1,580,DONKEY_T_MMAP) == 0", res == 0);

	res = donkey_publication(donkey, 2, 4096, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(2,4096,DONKEY_T_MMAP) == 0", res == 0);

	info = donkey_pub_info(donkey, 1);
	CuAssert(tc, "info.t_best == 100", info.t_best == 100);
	CuAssert(tc, "info.bps_best == 5800", info.bps_best == 5800);
	CuAssert(tc, "info.t_worst == 500", info.t_worst == 500);
	CuAssert(tc, "info.bps_worst == 1160", info.bps_worst == 1160);

	info = donkey_pub_info(donkey, 2);
	CuAssert(tc, "info.t_best == 400", info.t_best == 400);
	CuAssert(tc, "info.bps_best == 10690", info.bps_best == 10960);
	CuAssert(tc, "info.t_worst == 500", info.t_worst == 500);
	CuAssert(tc, "info.bps_worst == 8768", info.bps_worst == 8768);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc11_sub_mmap_info(CuTest* tc)
{
	int res;
	void* donkey;
	donkey_info_t info;
	uint16_t id;

	donkey = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey != NULL);

	res = donkey_subscribe(donkey, 1, 256, consume, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_sub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	res = donkey_subscribe(donkey, 2, 2596, consume, tc, DONKEY_T_MMAP_1S);
	CuAssert(tc, "donkey_sub(2,2596,DONKEY_T_MMAP_1S) == 0", res == 0);

	res = donkey_subscribe(donkey, 3, 580, consume, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_sub(3,580,DONKEY_T_MMAP) == 0", res == 0);

	res = donkey_subscribe(donkey, 4, 4096, consume, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_sub(4,4096,DONKEY_T_MMAP) == 0", res == 0);

	info = donkey_sub_info(donkey, 1);
	CuAssert(tc, "info.t_best == 100", info.t_best == 100);
	CuAssert(tc, "info.bps_best == 2560", info.bps_best == 2560);
	CuAssert(tc, "info.t_worst == 100", info.t_worst == 100);
	CuAssert(tc, "info.bps_worst == 2560", info.bps_worst == 2560);

	info = donkey_sub_info(donkey, 2);
	CuAssert(tc, "info.t_best == 1000", info.t_best == 1000);
	CuAssert(tc, "info.bps_best == 2596", info.bps_best == 2596);
	CuAssert(tc, "info.t_worst == 1000", info.t_worst == 1000);
	CuAssert(tc, "info.bps_worst == 2596", info.bps_worst == 2596);

	info = donkey_sub_info(donkey, 3);
	CuAssert(tc, "info.t_best == 100", info.t_best == 100);
	CuAssert(tc, "info.bps_best == 5800", info.bps_best == 5800);
	CuAssert(tc, "info.t_worst == 900", info.t_worst == 900);
	CuAssert(tc, "info.bps_worst == 644", info.bps_worst == 644);

	info = donkey_sub_info(donkey, 4);
	CuAssert(tc, "info.t_best == 800", info.t_best == 800);
	CuAssert(tc, "info.bps_best == 5760", info.bps_best == 5760);
	CuAssert(tc, "info.t_worst == 900", info.t_worst == 900);
	CuAssert(tc, "info.bps_worst == 5120", info.bps_worst == 5120);

	res = donkey_destroy(donkey);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc12_test_run(CuTest* tc)
{
	int res, i;
	void* donkey1;
	void* donkey2;
	donkey_state_t state;

	donkey1 = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey1 != NULL);

	res = donkey_subscribe(donkey1, 1, 256, consume, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_sub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	donkey2 = donkey_create(1, device2);
	CuAssert(tc, "donkey_create(1) != NULL", donkey2 != NULL);

	res = donkey_publication(donkey2, 1, 256, publish, tc, DONKEY_T_MMAP_100MS);
	CuAssert(tc, "donkey_pub(1,256,DONKEY_T_MMAP_100MS) == 0", res == 0);

	for (i=0; i < 10; i++) {
		res = donkey_sync(donkey1, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);

		res = donkey_sync(donkey2, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);
	}

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_ONLINE);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_ONLINE);

	donkey_reset(donkey1);

	res = donkey_sync(donkey1, 100);
	CuAssert(tc, "donkey_sync() == 0", res == 0);

	res = donkey_sync(donkey2, 100);
	CuAssert(tc, "donkey_sync() == 0", res == 0);

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_SYNC", state == DONKEY_ST_SYNC);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_SYNC", state == DONKEY_ST_SYNC);

	res = donkey_destroy(donkey1);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);

	res = donkey_destroy(donkey2);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc13_pubsub_mismatch(CuTest* tc)
{
	int res, i;
	void* donkey1;
	void* donkey2;
	donkey_state_t state;

	donkey1 = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey1 != NULL);

	res = donkey_subscribe(donkey1, 1, 2048, consume, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_sub(1,2048,DONKEY_T_MMAP) == 0", res == 0);

	donkey2 = donkey_create(1, device2);
	CuAssert(tc, "donkey_create(1) != NULL", donkey2 != NULL);

	res = donkey_publication(donkey2, 1, 1024, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(1,1024,DONKEY_T_MMAP) == 0", res == 0);

	for (i=0; i < 10; i++) {
		res = donkey_sync(donkey1, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);

		res = donkey_sync(donkey2, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);
	}

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	donkey_reset(donkey2);

	for (i=0; i < 10; i++) {
		res = donkey_sync(donkey1, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);

		res = donkey_sync(donkey2, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);
	}

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	res = donkey_destroy(donkey1);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);

	res = donkey_destroy(donkey2);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

void tc14_pubsub_recover(CuTest* tc)
{
	int res, i;
	void* donkey1;
	void* donkey2;
	donkey_state_t state;

	donkey1 = donkey_create(1, device1);
	CuAssert(tc, "donkey_create(1) != NULL", donkey1 != NULL);

	res = donkey_subscribe(donkey1, 1, 2048, consume, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_sub(1,2048,DONKEY_T_MMAP) == 0", res == 0);

	res = donkey_publication(donkey1, 2, 1024, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(2,1024,DONKEY_T_MMAP) == 0", res == 0);

	donkey2 = donkey_create(1, device2);
	CuAssert(tc, "donkey_create(1) != NULL", donkey2 != NULL);

	res = donkey_publication(donkey2, 1, 2048, publish, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_pub(1,2048,DONKEY_T_MMAP) == 0", res == 0);

	for (i=0; i < 10; i++) {
		res = donkey_sync(donkey1, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);

		res = donkey_sync(donkey2, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);
	}

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_OFFLINE);

	res = donkey_subscribe(donkey2, 2, 1024, consume, tc, DONKEY_T_MMAP);
	CuAssert(tc, "donkey_sub(2,1024,DONKEY_T_MMAP) == 0", res == 0);

	donkey_reset(donkey2);

	for (i=0; i < 10; i++) {
		res = donkey_sync(donkey1, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);

		res = donkey_sync(donkey2, 100);
		CuAssert(tc, "donkey_sync() == 0", res == 0);
	}

	state = donkey_get_state(donkey1);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_ONLINE);

	state = donkey_get_state(donkey2);
	CuAssert(tc, "state == DONKEY_ST_ONLINE", state == DONKEY_ST_ONLINE);

	res = donkey_destroy(donkey1);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);

	res = donkey_destroy(donkey2);
	CuAssert(tc, "donkey_destroy() == 0", res == 0);
}

CuSuite* donkey_test_suite(void)
{
	CuSuite* suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, tc1_create_delete);
	SUITE_ADD_TEST(suite, tc2_create_pub_100ms);
	SUITE_ADD_TEST(suite, tc3_create_pub_1s);
	SUITE_ADD_TEST(suite, tc4_create_publications);
	SUITE_ADD_TEST(suite, tc5_duplicate_publications);
	SUITE_ADD_TEST(suite, tc6_duplicate_subscriptions);
	SUITE_ADD_TEST(suite, tc7_pub_100ms_no_mem);
	SUITE_ADD_TEST(suite, tc8_pub_1s_no_mem);
	SUITE_ADD_TEST(suite, tc9_pub_mmap_info);
	SUITE_ADD_TEST(suite, tc10_pub_mmap_info);
	SUITE_ADD_TEST(suite, tc11_sub_mmap_info);
	SUITE_ADD_TEST(suite, tc12_test_run);
	SUITE_ADD_TEST(suite, tc13_pubsub_mismatch);
	SUITE_ADD_TEST(suite, tc14_pubsub_recover);
	return suite;
}

int main(void)
{
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();

	device1 = serdev_open("/dev/ttyS1");
	device2 = serdev_open("/dev/ttyS2");

	CuSuiteAddSuite(suite, donkey_test_suite());
	CuSuiteRun(suite);

	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);

	serdev_close(device1);
	serdev_close(device2);

	if (suite->failCount) {
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}


