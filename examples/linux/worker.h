#include <stdint.h>
#include <stdbool.h>

#include "donkey.h"

typedef struct {
    int id;
    int size;
    donkey_type_t type;    
} pubsub_t;

int
worker(char tag, FD fd, int session, uint32_t duration, bool log, 
       pubsub_t pubsub[]);

