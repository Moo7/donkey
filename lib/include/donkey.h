#ifndef DONKEY_H
#define DONKEY_H

#include "config.h"

#if HAVE_STDLIB_H
#include <stdlib.h>
#endif

#if HAVE_STDIO_H
#include <stdio.h>
#endif

#if HAVE_STRING_H
#include <string.h>
#endif

#if HAVE_STDDEF_H
#include <stddef.h>
#endif

#if HAVE_STDINT_H
#include <stdint.h>
#endif

#if HAVE_STDBOOL_H 
#include <stdbool.h>
#endif

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#if HAVE_WINDOWS_H
#include <windows.h>
#endif

#if HAVE_WINSOCK2_H
#include <winsock2.h>
#endif

#include "donkey_errno.h"

#ifdef NDEBUG
 #define DONKEY_EXCEPTION(ret,err,msg) \
	{ \
		donkey_errno = err; \
		fprintf(stderr, "%s %i: %i, %s\n", __FILE__, __LINE__, err, msg); \
	    return(ret); \
    }
#else
 #define DONKEY_EXCEPTION(ret,err,msg) \
	{ \
		donkey_errno = err; \
		fprintf(stderr, "%s %i: %i, %s\n", __FILE__, __LINE__, err, msg); \
		exit(err); \
	}
#endif

#define DONKEY_ERROR(ret,err,msg) \
	{ \
		donkey_errno = err; \
		fprintf(stderr, "%s %i: %i, %s\n", __FILE__, __LINE__, err, msg); \
		return(ret); \
	}

#ifdef NDEBUG
 #define DONKEY_WARNING(ret,err,msg) \
	{ \
		donkey_errno = err; \
		return(ret); \
	}
#else
 #define DONKEY_WARNING(ret,err,msg) \
	{ \
		donkey_errno = err; \
		fprintf(stderr, "%s %i: %i, %s\n", __FILE__, __LINE__, err, msg); \
		return(ret); \
	}
#endif

#define DONKEY_MIN_ID		1
#define DONKEY_MAX_ID		0x7FFF
#define DONKEY_MIN_SESSION	1
#define DONKEY_MAX_SESSION	0x0FFF

#define DONKEY_ID_NONE		0
#define DONKEY_TIMEOUT 		30 // 3s

#ifdef __linux__
#define FD int
#endif


/**
 * Definition of data types that can be transferred across a donkey trail
 *
 * PERIODIC: MMAP_1S/MMAP_100MS/MMAP_10MS/MMAP_1MS
 * Data will arrive within or up to the maximum period time that has been
 * specified for the connection. The size of the data used for periodic
 * transfers must be specified during system start and will be verified against
 * the maximum available bandwidth (e.g. 256Bytes @100ms). Furthermore
 * allocating a size for periodic data transfer will reduce the remaining
 * available bandwidth for 'normal' data transfers as well as periodic transfer
 * having a lower refresh rate (i.e. larger period).
 *
 * ASAP: MMAP
 * Data will be send as soon as possible using the available bandwidth provided
 * by the physical link (e.g. serial connection @115K2 BAUD). However there is
 * no guarantee as to 'when' the data will have arrived. The time needed for
 * the of a block of data depends on the available bandwidth and the utilization
 * of that bandwidth.
 *
 * Note that some periods may not be supported due to bandwidth limitations on
 * the physical link layer.
 */
typedef enum {
	DONKEY_T_MMAP	    = 1,	/** will be synchronized as soon as possible */
	DONKEY_T_MMAP_1S	= 2,	/** will be synchronized every 1s */
	DONKEY_T_MMAP_100MS = 3,	/** will be synchronized every 100ms */
    //TODO: not supported yet: DONKEY_T_MMAP_10MS  = 4,	/** will be synchronized every 10ms */
	//TODO: not supported yet: DONKEY_T_MMAP_1MS   = 5,	/** will be synchronized every 1ms */
} donkey_type_t;

/**
 * Donkey (logical) link states
 */
typedef enum {
	DONKEY_ST_UNDEFINED = -1,
	DONKEY_ST_SYNC		= 0,
	DONKEY_ST_START		= 1,
	DONKEY_ST_ONLINE 	= 2,
	DONKEY_ST_OFFLINE	= 3,
} donkey_state_t;

/**
 * Contains information on the best- and worst case synchronization
 * times as well as bytes per second (bps) data transfer for a requested
 * publisher or subscriber.
 */
typedef struct {
	int32_t		t_best;		/** optimal synchronization time in [ms] (-1 == never) */
	int32_t		t_worst;	/** worst synchronization time in [ms] (-1 == never) */
	size_t		bps_best;	/** optimal transfer rate in bytes per second */
	size_t		bps_worst;	/** worst transfer rate in bytes per second */
    size_t      bps_raw; // TODO: optimal from device?
} donkey_info_t;

typedef ssize_t (*donkey_f_read)(FD fd, void *buf, size_t size);
typedef ssize_t (*donkey_f_write)(FD fd, const void *buf, size_t size);

/**
 * Contains file descriptor and operations for reading from and writing 
 * to a (serial) device.
 */
typedef struct {
	donkey_f_read	read;
	donkey_f_write	write;
	FD              fd;
} donkey_dev_t;

typedef bool (*donkey_f_pub)(donkey_type_t type, uint16_t id, size_t size, void *data, void *user);
typedef void (*donkey_f_sub)(donkey_type_t type, uint16_t id, size_t size, void *data, void *user);

extern int donkey_errno;

void *donkey_create(uint16_t id, donkey_dev_t device);
int donkey_destroy(void *donkey);

int donkey_publication(void *donkey, uint16_t id, size_t size, donkey_f_pub publish, void *user, donkey_type_t type);
int donkey_subscribe(void *donkey, uint16_t id, size_t size, donkey_f_sub consume, void *user, donkey_type_t type);
size_t donkey_pub_avail(void *donkey, donkey_type_t type);
size_t donkey_sub_avail(void *donkey, donkey_type_t type);
donkey_info_t donkey_pub_info(void *donkey, uint16_t id);
donkey_info_t donkey_sub_info(void *donkey, uint16_t id);

int donkey_sync(void *donkey, uint32_t t_elapsed);
donkey_state_t donkey_get_state(void *donkey);
bool donkey_is_connected(void *donkey);
bool donkey_is_online(void *donkey);
void donkey_reset(void *donkey);

uint8_t *donkey_get_uint16(uint8_t *data, uint16_t *val);
uint8_t *donkey_add_uint16(uint8_t *data, uint16_t val);
uint8_t *donkey_get_uint32(uint8_t *data, uint32_t *val);
uint8_t *donkey_add_uint32(uint8_t *data, uint32_t val);

#endif // DONKEY_H

