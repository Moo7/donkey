#include "config.h"

#if HAVE_STDLIB_H
#include <stdlib.h>
#endif

#if HAVE_STRING_H
#include <string.h>
#endif

#if HAVE_STDBOOL_H
#include <stdbool.h>
#endif

#if HAVE_STDINT_H
#include <stdint.h>
#endif

#if HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#include "crc16_add.h"
#include "donkey_mmap.h"


/**
 * (private) definition of a memory map instance
 */
typedef struct {
	size_t			size;
	uint8_t*		start;
	uint8_t*		end;
	uint8_t*		curr;
	uint8_t*		sync;
	uint32_t		segment;
	uint16_t		nrel;
	bool			atomic;
	donkey_crc_t	crc;
} donkey_mmap_t;

/**
 * Create a new memory map instance
 *
 * @param	size	total size in bytes of the mmap
 * @return			handle to mmap instance, or NULL in case of failures
 */
void* donkey_mmap_create(size_t size)
{
	donkey_mmap_t* self = calloc(1, sizeof(donkey_mmap_t));
	if (!self) {
		return NULL;
	}

	self->size = size;
	self->start = calloc(size, sizeof(uint8_t));
	if (!self->start) {
		free(self);
		return NULL;
	}

	self->end = self->start + size;
	self->curr = self->start;
	self->sync = self->start;
	self->segment = 0;
	self->nrel = 0;
	self->atomic = false;
	self->crc.id = 0;
	self->crc.size = 0;
	return self;
}

/**
 * Create a new memory map instance for a single publisher/subscriber
 * the entire mmap will be allocated on creation.
 *
 * @param	size	total size in bytes of the mmap
 * @param	id		unique identifier of the mmap owner
 * @return			handle to mmap instance, or NULL in case of failures
 */
void* donkey_mmap_atomic(size_t size, uint16_t id)
{
	donkey_mmap_t* self = (donkey_mmap_t*)donkey_mmap_create(size);
	if (!self) {
		return NULL;
	}
	self->curr = self->end;
	self->atomic = true;
	
	id = htons(id);
	size = htons(size);
		
	self->crc.id = crc16_add((char*)&id, sizeof(id), 0);
	self->crc.size = crc16_add((char*)&size, sizeof(size), 0);
	return self;
}

/**
 * Destroys a memory map instance
 *
 * @param	mmap	handle to mmap instance
 * @return			void
 */
void donkey_mmap_destroy(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	free(self->start);
	free(self);
}

/**
 * Allocates space for publisher or subscriber on the mmap.
 *
 * @param	mmap	handle to mmap instance
 * @param	id		identifier of the publisher/subscriber
 * @param	size	size in bytes of the data to be allocated.
 * @return			pointer to the allocated data, or NULL
 *					in case of failure.
 */
void* donkey_mmap_alloc(void* mmap, uint16_t id, size_t size)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	uint16_t len = size;
	void* data;
	size_t alloc = sizeof(id) + sizeof(len) + size;

	if (self->atomic) {
		return NULL;
	}

	if ( (self->curr + alloc) > self->end ) {
		return NULL;
	}
	
	id = htons(id);
	memcpy(self->curr, &id, sizeof(id));
	self->curr += sizeof(id);

	len = htons(len);
	memcpy(self->curr, &len, sizeof(len));
	self->curr += sizeof(len);

	data = self->curr;
	self->curr += size;
	self->nrel;

	id = htons(id);
	size = htonl(size);
	
	self->crc.id = crc16_add((char*)&id, sizeof(id), self->crc.id);
	self->crc.size = crc16_add((char*)&size, sizeof(size), self->crc.size);
	return data;
}

/**
 * Returns the remaining number of bytes that can be allocated using 
 * donkey_mmap_alloc().
 *
 * @param	mmap	handle to mmap instance
 * @return			remaining size in bytes
 */
size_t donkey_mmap_avail(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	size_t size;

	size = self->end - self->curr;
	if (!self->atomic) {
		size -= (sizeof(uint16_t) + sizeof(uint16_t)); // subtract size of id and len
	}
	return size;
}

/**
 * Returns size in bytes of the memory map.
 *
 * @param	mmap	handle to mmap instance
 * @return			size in bytes
 */
size_t donkey_mmap_size(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	return (self->curr - self->start);
}

/**
 * Returns the number of elements (i.e. number of allocations on this mmap).
 *
 * @param	mmap	handle to mmap instance
 * @return			number of elements
 */
uint16_t donkey_mmap_nrel(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	return self->nrel;
}

/**
 * Returns a pointer to the start of the actual memory map.
 *
 * @param	mmap	handle to mmap instance
 * @return			pointer to mmap data
 */
uint8_t* donkey_mmap_data(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	return self->start;
}

/**
 * Request for synchronization of (a part of) a memory map.
 *
 * @param	mmap	handle to mmap instance
 * @param	size	size in bytes of the memory to be synchronized 
 * @return			struct containing:
 *					- pointer to the segment of the mmap to be synchronized
 *					- actual size in bytes of the segment
 *					- actual synchronization frame number [0 ... x]
 */
donkey_sync_t donkey_mmap_sync(void* mmap, size_t size)
{
	donkey_mmap_t* self = (void*)mmap;
	donkey_sync_t sync;

	if (self->sync >= self->end) {
		self->sync = self->start;
		self->segment = 0;
	}

	if (size > (self->end - self->sync)) {
		size = self->end - self->sync;
	}

	sync.segment = self->segment;
	sync.data = self->sync;
	sync.size = size;

	self->sync += size;
	self->segment++;
	return sync;
}

/**
 * Resets the memory map synchronization sequence.
 *
 * @param	mmap	handle to mmap instance
 * @return			void
 */
void donkey_mmap_reset(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	self->sync = self->start;
	self->segment = 0;
}

/**
 * Returns true when the entire memory has been synchronized.
 *
 * @param	mmap	handle to mmap instance
 * @return			see description
 */
bool donkey_mmap_is_synced(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	return (self->sync == self->start || self->sync >= self->end);
}

/**
 * Returns an identifier and size cyclic redundancy checksum
 * 
 *
 * @param	mmap	handle to mmap instance
 * @return			see description
 */
donkey_crc_t donkey_mmap_crc(void* mmap)
{
	donkey_mmap_t* self = (donkey_mmap_t*)mmap;
	return self->crc;
}

