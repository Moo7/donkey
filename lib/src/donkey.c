#include "donkey_link.h"
#include "donkey.h"

/** Contains publisher information */
typedef struct donkey_pub {
	uint16_t			id;
	donkey_type_t		type;
	size_t				size;
	donkey_f_pub		publish;
	void*				user;
	void*				data;
	struct donkey_pub*	next;
} donkey_pub_t;

/** Contains subscriber information */
typedef struct donkey_sub {
	uint16_t			id;
	donkey_type_t		type;
	size_t				size;
	donkey_f_sub		consume;
	void*				user;
	void*				data;
	struct donkey_sub*	next;
} donkey_sub_t;

/** Definition of a donkey communication instance */
typedef struct {
	donkey_pub_t*       pub;
	donkey_sub_t*       sub;
	void*               session;
	void*               link;
} donkey_t;

/**
 * Contains last error that occured, same behavior and codes as 'errno'.
 */
int donkey_errno = 0;

/**
 * Callback that will be invoked when new publication data can be
 * send to the remote peer.
 *
 * @param	donkey	donkey instance
 * @param	type	publisher type
 * @param	id		publisher identifier
 * @return			returns true if one or more publisher has data changes
 */
static bool donkey_publish(void* donkey, donkey_type_t type, uint16_t id)
{
	donkey_t* self = (donkey_t*) donkey;
	donkey_pub_t* pub;
	bool changed = false;

	for (pub = self->pub; pub; pub = pub->next) {
		if (pub->type == type && (pub->id == id || id == DONKEY_ID_NONE)) {
			changed |= pub->publish(type, pub->id, pub->size, pub->data, pub->user);
		}
	}
	return changed;
}

/**
 * Callback that will be invoked when new publication data from the
 * remote peer has arrived.
 *
 * @param	donkey	donkey instance
 * @param	type	publisher type
 * @param	id		publisher identifier
 * @return			void
 */
static void donkey_consume(void* donkey, donkey_type_t type, uint16_t id)
{
	donkey_t* self = (donkey_t*) donkey;
	donkey_sub_t* sub;

	for (sub = self->sub; sub; sub = sub->next) {
		if (sub->type == type && (sub->id == id || id == DONKEY_ID_NONE)) {
			sub->consume(type, sub->id, sub->size, sub->data, sub->user);
		}
	}
}

/**
 * Creates a donkey communication instance used for synchronisation of memory
 * mapped- and serialized data with a remote peer using a serial connection.
 *
 * @param	id		unique link identifier
 * @param	device	file descriptor and operations for reading from- and 
 *                  writing to a serial device
 * @param	write	function pointer used to write to the serial device
 * @return			donkey instance or NULL in case of failure
 */
void* donkey_create(uint16_t id, donkey_dev_t device)
{
	donkey_t* self = NULL;

	if ( (id < DONKEY_MIN_SESSION) || (id > DONKEY_MAX_SESSION) ) {
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "illegal link identifier");
	}

	if (!device.fd || !device.read || !device.write) {
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "illegal device value (fd|read|write)");
	}

	self = (donkey_t*) calloc(1, sizeof(donkey_t));
	self->pub = NULL;
	self->sub = NULL;

	self->session = donkey_session_create(DONKEY_MMAP_100MS, DONKEY_MMAP_1S);
	if (!self->session) {
		free(self);
		return NULL;
	}

	self->link = donkey_link_create(id, device, self->session, donkey_publish, donkey_consume, self);
	if (!self->link) {
		donkey_session_destroy(self->session);
		free(self);
		return NULL;
	}

	donkey_link_set_timeout(self->link, DONKEY_TIMEOUT);
	return self;
}

/**
 * Destroys a donkey instance.
 *
 * @param	donkey	donkey instance
 * @return			returns non-zero on failure
 */
int donkey_destroy(void* donkey)
{
	int res1, res2;
	donkey_t* self = (donkey_t*) donkey;
	donkey_pub_t *p1, *p2;
	donkey_sub_t *s1, *s2;

	if (!self) {
		DONKEY_EXCEPTION(-1, DONKEY_EINVAL, "illegal donkey instance");
	}

	for (p1 = self->pub; p1; ) {
		if (p1->next) {
			p2 = p1;
			p1 = p1->next;
			free(p2);
		}
		else {
			free(p1);
			p1 = NULL;
		}
	}

	for (s1 = self->sub; s1; ) {
		if (s1->next) {
			s2 = s1;
			s1 = s1->next;
			free(s2);
		}
		else {
			free(s1);
			s1 = NULL;
		}
	}

	res1 = donkey_session_destroy(self->session);
	res2 = donkey_link_destroy(self->link);
	free(self);
	return (res1 || res2) ? -1 : 0;
}

/**
 * Adds a new publisher to donkey. Its data will be synchronized with the
 * matching subcriber on the remote peer.
 *
 * @param	donkey	donkey instance
 * @param	id		unique publisher identifier
 * @param	size	size in bytes of the data being published
 * @param	publish	function to be called when publishing the data
 * @param	user	reference to some user data (e.g. object instance)
 * @param	type	publication type (e.g. 1S_MMAP)
 * @return			returns non-zero on failure
 */
int donkey_publication(void* donkey, uint16_t id, size_t size, donkey_f_pub publish, void* user, donkey_type_t type)
{
	donkey_t* self = (donkey_t*)donkey;
	donkey_pub_t* pub;
	void* data;

	if ( !self || (id < DONKEY_MIN_ID) || (id > DONKEY_MAX_ID) || !size || !publish ) {
		DONKEY_EXCEPTION(-1, DONKEY_EINVAL, "illegal value for new publisher");
	}

	for (pub = self->pub; pub != NULL; pub = pub->next) {
		if (pub->id == id) {
			DONKEY_ERROR(-1, DONKEY_EEXIST, "publisher already exists");
		}
	}

	data = donkey_session_publication(self->session, id, size, type);
	if (!data) {
		return -1;
	}

	// insert new publication at start of linked list
	pub = (donkey_pub_t*)malloc(sizeof(donkey_pub_t));
	pub->type = type;
	pub->id = id;
	pub->size = size;
	pub->publish = publish;
	pub->user = user;
	pub->data = data;
	pub->next = self->pub;
	self->pub = pub;
	return 0;
}

/**
 * Adds a new subscription to donkey. Its data will be synchronized with the
 * matching publisher on the remote peer.
 *
 * @param	donkey	donkey instance
 * @param	id		unique publisher identifier
 * @param	size	size in bytes of the data being published
 * @param	publish	function to be called when the published data is available
 * @param	user	reference to some user data (e.g. object instance)
 * @param	type	publication type (e.g. 1S_MMAP)
 * @return			returns non-zero on failure
 */
int donkey_subscribe(void* donkey, uint16_t id, size_t size, donkey_f_sub consume, void* user, donkey_type_t type)
{
	donkey_t* self = (donkey_t*)donkey;
	donkey_sub_t* sub;
	void* data;

	if ( !self || (id < DONKEY_MIN_ID) || (id > DONKEY_MAX_ID) || !size || !consume ) {
		DONKEY_EXCEPTION(-1, DONKEY_EINVAL, "illegal value for new subscription");
	}

	for (sub = self->sub; sub != NULL; sub = sub->next) {
		if (sub->id == id) { // identifier already exists; already subscribed to
			DONKEY_ERROR(-1, DONKEY_EEXIST, "subscription already exists");
		}
	}

	data = donkey_session_subscription(self->session, id, size, type);
	if (!data) {
		return -1;
	}

	// insert new subscription at start of linked list
	sub = (donkey_sub_t*)malloc(sizeof(donkey_sub_t));
	sub->type = type;
	sub->id = id;
	sub->size = size;
	sub->consume = consume;
	sub->user = user;
	sub->data = data;
	sub->next = self->sub;
	self->sub = sub;
	return 0;
}

/**
 * Returns the remaining size in bytes for the given publication type
 *
 * @param	donkey	donkey instance
 * @param	type	kind of data being published
 * @return			see description
 */
size_t donkey_pub_avail(void* donkey, donkey_type_t type)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return 0;
	}
	return donkey_session_pub_avail(self->session, type);
}

/**
 * Returns the remaining size in bytes for the given subscripion type
 *
 * @param	donkey	donkey instance
 * @param	type	kind of data being subscribed to
 * @return			see description
 */
size_t donkey_sub_avail(void* donkey, donkey_type_t type)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return 0;
	}
	return donkey_session_sub_avail(self->session, type);
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested publisher.
 *
 * @param	donkey	donkey instance
 * @param	id	    publisher identifier
 * @return			returns publication information
 */
donkey_info_t donkey_pub_info(void* donkey, uint16_t id)
{
    donkey_info_t info;
	donkey_t* self = (donkey_t*)donkey;
	donkey_pub_t* pub;

    info.t_best = info.t_worst = -1;
    info.bps_best = info.bps_worst = info.bps_raw = 0;

	if (!self) {
		DONKEY_EXCEPTION(info, DONKEY_EINVAL, "illegal donkey instance");
	}

	for (pub=self->pub; pub && pub->id != id; pub=pub->next) {
		/* noop */
	}
	if (!pub) {
		DONKEY_ERROR(info, DONKEY_ENOENT, "no such publisher");
	}
	return donkey_link_pub_info(self->link, pub->id, pub->size, pub->type);
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested subscription.
 *
 * @param	donkey	donkey instance
 * @param	id		subscription identifier
 * @return			returns subscription information
 */
donkey_info_t donkey_sub_info(void* donkey, uint16_t id)
{
    donkey_info_t info;
	donkey_t* self = (donkey_t*)donkey;
	donkey_sub_t* sub;

    info.t_best = info.t_worst = -1;
    info.bps_best = info.bps_worst = info.bps_raw = 0;

	if (!self) {
		return info;
	}

	for (sub=self->sub; sub && sub->id != id; sub=sub->next) {
		/* noop */
	}
	if (!sub) {
		DONKEY_ERROR(info, DONKEY_ENOENT, "no such subscription");
	}
	return donkey_link_sub_info(self->link, sub->id, sub->size, sub->type);
}

/**
 * Synchronize published data across the donkey route
 *
 * @param	donkey		donkey instance
 * @param	t_elapsed	time elapsed (ms) since last call to sync
 * @return				see description
 */
int donkey_sync(void* donkey, uint32_t t_elapsed)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		DONKEY_EXCEPTION(-1, DONKEY_EINVAL, "illegal donkey instance");
	}
	return donkey_link(self->link, t_elapsed);
}

/**
 * Returns the current (logical) connection state
 *
 * @param	donkey	donkey instance
 * @return			current state
 */
donkey_state_t donkey_get_state(void* donkey)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return DONKEY_ST_UNDEFINED;
	}
	return donkey_link_get_state(self->link);
}

/**
 * Returns true when there is some form of data
 * transmission on the link layer.
 *
 * @param	donkey	donkey instance
 * @return			current state
 */
bool donkey_is_connected(void* donkey)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return false;
	}
	return donkey_link_is_connected(self->link);
}

/**
 * Returns true when the link layer state
 * is online (i.e. pub/sub's are being synced)
 *
 * @param	donkey	donkey instance
 * @return			current state
 */
bool donkey_is_online(void* donkey)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return false;
	}
	return donkey_link_is_online(self->link);
}

/**
 * Resets the donkey trail to pristine state
 *
 * @param	donkey	donkey instance
 * @return			void
 */
void donkey_reset(void* donkey)
{
	donkey_t* self = (donkey_t*)donkey;

	if (!self) {
		return;
	}
	donkey_link_reset(self->link);
}

/**
 * Gets a short integer from the given data section of a frame and returns
 * the new offset of the data section.
 *
 * @param	data	data section of a frame instance
 * @param	val		value to be stored in the data section
 * @return			pointer to remaining space on the frame data section
 */
uint8_t* donkey_get_uint16(uint8_t* data, uint16_t* val)
{
	size_t size = sizeof(uint16_t);
	memcpy(val, data, size);
	*val = htons(*val);
	return (data + size);
}

/**
 * Adds a short integer to the given data section of a frame and returns
 * the new offset of the data section.
 *
 * @param	data	data section of a frame instance
 * @param	val		value to be stored in the data section
 * @return			pointer to remaining space on the frame data section
 */
uint8_t* donkey_add_uint16(uint8_t* data, uint16_t val)
{
	size_t size = sizeof(uint16_t);
	val = htons(val);
	memcpy(data, &val, size);
	return (data + size);
}

/**
 * Gets a long integer from the given data section of a frame and returns
 * the new offset of the data section.
 *
 * @param	data	data section of a frame instance
 * @param	val		value to be stored in the data section
 * @return			pointer to remaining space on the frame data section
 */
uint8_t* donkey_get_uint32(uint8_t* data, uint32_t* val)
{
	size_t size = sizeof(uint32_t);
	memcpy(val, data, size);
	*val = htonl(*val);
	return (data + size);
}

/**
 * Adds a long integer to the given data section of a frame and returns

 * the new offset of the data section.
 *
 * @param	data	data section of a frame instance
 * @param	val		value to be stored in the data section

 * @return			pointer to remaining space on the frame data section
 */
uint8_t* donkey_add_uint32(uint8_t* data, uint32_t val)
{
	size_t size = sizeof(uint32_t);
	val = htonl(val);
	memcpy(data, &val, size);
	return (data + size);
}


