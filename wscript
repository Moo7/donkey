#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
import waftools

top='.'
out='build'


APPNAME='donkey'
VERSION='0.0.3'


def options(opt):
	opt.load('ccenv', tooldir=waftools.location)
	opt.add_option('--sertest', dest='sertest', default=False, action='store_true', help='Test local using pipes')


def configure(conf):
	conf.load('ccenv')

	conf.env.append_unique('INCLUDES', conf.path.find_node(out).abspath())
	conf.check(header_name='stdlib.h', features='c cprogram')
	conf.check(header_name='stdio.h', features='c cprogram')
	conf.check(header_name='string.h', features='c cprogram')
	conf.check(header_name='stddef.h', features='c cprogram')
	conf.check(header_name='stdbool.h', features='c cprogram')
	conf.check(header_name='stdint.h', features='c cprogram')
	conf.check(header_name='unistd.h', features='c cprogram', mandatory=False)
	conf.check(header_name='fcntl.h', features='c cprogram', mandatory=False)
	conf.check(header_name='termios.h', features='c cprogram', mandatory=False)
	conf.check(header_name='arpa/inet.h', features='c cprogram', mandatory=False)
	conf.check(header_name='windows.h', features='c cprogram', mandatory=False)
	conf.check(header_name='winsock2.h', features='c cprogram', mandatory=False)
	conf.write_config_header('config.h')

	if conf.options.sertest:
	    conf.env.append_unique('DEFINES', 'DONKEY_SERTEST')


def build(bld):
	bld.recurse('lib')
	bld.recurse('test')
	bld.recurse('examples')

