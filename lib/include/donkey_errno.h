#ifndef DONKEY_ERRNO_H
#define DONKEY_ERRNO_H

#include <errno.h>

#ifdef __WIN32__
#define ENODATA     61  // No data available 
#define ECOMM       70  // Communication error on send
#define EPROTO      71  // Protocol error 
#define EBADMSG     74  // Not a data message
#define EOVERFLOW   75  // Value too large for defined data type
#endif

#define DONKEY_EFAULT       EFAULT  // unrecoverable link error
#define DONKEY_ENOMEM       ENOMEM
#define DONKEY_ECOMM        ECOMM   // communication error on send
#define DONKEY_ENODATA      ENODATA
#define DONKEY_EBADMSG      EBADMSG // unknown or illegal message received
#define DONKEY_EINVAL       EINVAL
#define DONKEY_EEXIST       EEXIST // entity already exists (e.g. subscription)
#define DONKEY_ENOENT       ENOENT

#endif // DONKEY_ERRNO_H

