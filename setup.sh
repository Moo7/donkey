#!/bin/bash
set -e

if hash dnf 2>/dev/null
then
    sudo dnf install -y "waf-python3" "python3-pip"
    
elif hash yum 2>/dev/null
then
    sudo yum install -y "waf-python3" "python3-pip"
fi

pip3 install waftools --user
