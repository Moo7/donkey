#include "crc16_add.h"

#define POLY 0x8408

unsigned short crc16_add(char* p, unsigned short length, unsigned short crc)
{
	unsigned char i;
	unsigned int data;

	if (!crc) {
		crc = 0xffff;
	}

	do {
		for (i=0, data=(unsigned int)0xff & *p++; i < 8; i++, data >>= 1) {
			if ((crc & 0x0001) ^ (data & 0x0001)) {
				crc = (crc >> 1) ^ POLY;
			}
            else {
				crc >>= 1;
			}
		}
	} while (--length);

	crc = ~crc;
	data = crc;
	crc = (crc << 8) | (data >> 8 & 0xff);

	return (crc);
}

