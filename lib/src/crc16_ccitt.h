
/**
 *	CRC-CCITT (16bits)
 *                                       16   12   5
 *  this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
 *  This works out to be 0x1021, but the way the algorithm works
 *  lets us use 0x8408 (the reverse of the bit pattern).  The high
 *  bit is always assumed to be set, thus we only use 16 bits to
 *  represent the 17 bit value.
 *
 *  author: http://www8.cs.umu.se/~isak/snippets/crc-16.c
 */

unsigned short crc16_ccitt(char *data_p, unsigned short length);
