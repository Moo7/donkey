#include "config.h"
#include <stdio.h>
#include <string.h>

#if HAVE_STDINT_H
#include <stdint.h>
#endif

#include "crc16_add.h"
#include "donkey_mmap.h"
#include "donkey_session.h"
#include "donkey.h"

/**
 * (private) definition of a session instance
 */
typedef struct {
	donkey_ll_t			pub_100ms;
	donkey_ll_t			sub_100ms;
	donkey_ll_t			pub_1s;
	donkey_ll_t			sub_1s;

	donkey_ll_t*		pub;
	donkey_ll_t*		pub_sync;
	bool				pub_sync_complete;
	donkey_ll_t*		sub;
	donkey_ll_t*		sub_sync;

	uint32_t			pub_size;
	uint16_t			pub_nrel;
	uint32_t			sub_size;
	uint16_t			sub_nrel;
} donkey_session_t;

/**
 * Creates a new session instance
 *
 * @param	owner		reference to the owner creating this link
 * @param	size_100ms	total size in bytes of the 100ms memory map
 * @param	size_1s		total size in bytes of the 1s memory map
 * @return				returns session instance or NULL in case of failure
 */
void* donkey_session_create(size_t size_100ms, size_t size_1s)
{
	donkey_session_t* self;

	self = calloc(1, sizeof(donkey_session_t));
	if (!self) {
		DONKEY_EXCEPTION(NULL, DONKEY_ENOMEM, "failed to create session");
	}

	self->pub_100ms.mmap = donkey_mmap_create(size_100ms);
	self->sub_100ms.mmap = donkey_mmap_create(size_100ms);
	self->pub_1s.mmap = donkey_mmap_create(size_1s);
	self->sub_1s.mmap = donkey_mmap_create(size_1s);

	self->pub = NULL;
	self->pub_sync = NULL;
	self->pub_sync_complete = true;
	self->sub = NULL;
	self->sub_sync = NULL;

	if (!self->pub_100ms.mmap || !self->sub_100ms.mmap || !self->pub_1s.mmap || !self->sub_1s.mmap) {
		donkey_session_destroy(self);
		DONKEY_EXCEPTION(NULL, DONKEY_ENOMEM, "failed to create session mmap");
	}
	return self;
}

/**
 * Destroys a session instance
 *
 * @param	session	session instance
 * @return			returns non-zero on failure
 */
int donkey_session_destroy(void* session)
{
	donkey_session_t* self = (donkey_session_t*)session;
	donkey_ll_t *i1, *i2;

	for (i1 = self->pub; i1; ) {
		if (i1->next) {
			i2 = i1;
			i1 = i1->next;
			free(i2);
		}
		else {
			free(i1);
			i1 = NULL;
		}
	}

	for (i1 = self->sub; i1; ) {
		if (i1->next) {
			i2 = i1;
			i1 = i1->next;
			free(i2);
		}
		else {
			free(i1);
			i1 = NULL;
		}
	}

	donkey_mmap_destroy(self->pub_100ms.mmap);
	donkey_mmap_destroy(self->sub_100ms.mmap);
	donkey_mmap_destroy(self->pub_1s.mmap);
	donkey_mmap_destroy(self->sub_1s.mmap);
	free(self);
	return 0;
}

/**
 * Resets the current session to pristine state
 *
 * @param	session	session instance
 * @return			void
 */
void donkey_session_reset(void* session)
{
	donkey_session_t* self = (donkey_session_t*)session;

	self->pub_sync_complete = true;
	donkey_mmap_reset(self->pub_100ms.mmap);
	donkey_mmap_reset(self->sub_100ms.mmap);
	donkey_mmap_reset(self->pub_1s.mmap);
	donkey_mmap_reset(self->sub_1s.mmap);
	if (self->pub_sync) {
		donkey_mmap_reset(self->pub_sync->mmap);
	}
	if (self->sub_sync) {
		donkey_mmap_reset(self->sub_sync->mmap);
	}
}

/**
 * Returns the first element of a list of publisher of the requested
 * type.
 *
 * @param	session	session instance
 * @param	type	publisher type for which a list should be returned
 * @return			first element in list. returns NULL when empty or in case
 *                  of errors
 */
donkey_ll_t* donkey_session_pub(void* session, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	donkey_ll_t* pub;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		pub = &(self->pub_100ms);
		break;
	case DONKEY_T_MMAP_1S:
		pub = &(self->pub_1s);
		break;
	case DONKEY_T_MMAP:
		pub = self->pub;
		break;
	default:
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "invalid publisher type");
	}
	return pub;
}

/**
 * Returns the first element of a list of subscribers of the requested
 * type.
 *
 * @param	session	session instance
 * @param	type	subscriber type for which a list should be returned
 * @return			first element in list. returns NULL when empty or in case
 *                  of errors
 */
donkey_ll_t* donkey_session_sub(void* session, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	donkey_ll_t* sub;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		sub = &(self->sub_100ms);
		break;
	case DONKEY_T_MMAP_1S:
		sub = &(self->sub_1s);
		break;
	case DONKEY_T_MMAP:
		sub = self->sub;
		break;
	default:
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "invalid subscriber type");
	}
	return sub;
}

/**
 * Adds a publication to be published to the remote peer
 *
 * @param	session	session instance
 * @param	id		unique publisher identifier
 * @param	size	size in bytes of the data being published
 * @param	type	kind of data being published
 * @return			returns pointer to data section or NULL in case of failure
 */
void* donkey_session_publication(void* session, uint16_t id, size_t size, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	void* data = NULL;
	donkey_ll_t* pub;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		data = donkey_mmap_alloc(self->pub_100ms.mmap, id, size);
		break;
	case DONKEY_T_MMAP_1S:
		data = donkey_mmap_alloc(self->pub_1s.mmap, id, size);
		break;
	case DONKEY_T_MMAP:
		pub = calloc(1,sizeof(donkey_ll_t));
		pub->id = id;
		pub->mmap = donkey_mmap_atomic(size, id);
		data = donkey_mmap_data(pub->mmap);
		pub->next = self->pub;
		self->pub = pub;
		if (!pub->next) {
			self->pub_sync = pub;
		}
		self->pub_nrel += 1;
		self->pub_size += size;
		break;
	default:
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "invalid publication type");
	}

	if (!data) {
		DONKEY_ERROR(NULL, DONKEY_ENOMEM, "failed to add publication");
	}
	return data;
}

/**
 * Returns the remaining size in bytes for the given publication type
 *
 * @param	session	session instance
 * @param	type	kind of data being published
 * @return			see description
 */
size_t donkey_session_pub_avail(void* session, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	size_t size;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		size = donkey_mmap_avail(self->pub_100ms.mmap);
		break;
	case DONKEY_T_MMAP_1S:
		size = donkey_mmap_avail(self->pub_1s.mmap);
		break;
	case DONKEY_T_MMAP:
		size = (size_t)(-1);
		break;
	default:
		size = 0;
		break;
	}
	return size;
}

/**
 * Subscribes to a publication on the remote peer
 *
 * @param	session	session instance
 * @param	id		unique publisher identifier
 * @param	size	size in bytes of the data being published
 * @param	type	kind of data being published
 * @return			returns pointer to data section or NULL in case of failure
 */
void* donkey_session_subscription(void* session, uint16_t id, size_t size, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	void* data;
	donkey_ll_t* sub;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		data = donkey_mmap_alloc(self->sub_100ms.mmap, id, size);
		break;
	case DONKEY_T_MMAP_1S:
		data = donkey_mmap_alloc(self->sub_1s.mmap, id, size);
		break;
	case DONKEY_T_MMAP:
		sub = calloc(1,sizeof(donkey_ll_t));
		sub->id = id;
		sub->mmap = donkey_mmap_atomic(size, id);
		data = donkey_mmap_data(sub->mmap);
		sub->next = self->sub;
		self->sub = sub;
		if (!sub->next) {
			self->sub_sync = sub;
		}
		self->sub_nrel += 1;
		self->sub_size += size;
		break;
	default:
		DONKEY_EXCEPTION(NULL, DONKEY_EINVAL, "invalid subscription type");
	}

	if (!data) {
		DONKEY_ERROR(NULL, DONKEY_ENOMEM, "failed to subscribe to publication");
	}
	return data;
}

/**
 * Returns the remaining size in bytes for the given subscripion type
 *
 * @param	session	session instance
 * @param	type	kind of data being subscribed to
 * @return			see description
 */
size_t donkey_session_sub_avail(void* session, donkey_type_t type)
{
	donkey_session_t* self = (donkey_session_t*)session;
	size_t size;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		size = donkey_mmap_avail(self->sub_100ms.mmap);
		break;
	case DONKEY_T_MMAP_1S:
		size = donkey_mmap_avail(self->sub_1s.mmap);
		break;
	case DONKEY_T_MMAP:
		size = (size_t)(-1);
		break;
	default:
		size = 0;
		break;
	}
	return size;
}

/**
 * Returns the next element in the linked list of publications
 *
 * @param	start	first element in the linked list
 * @param	pub		current element in the linked list
 * @return			next element in the linked list
 */
static donkey_ll_t* donkey_session_pub_next(donkey_ll_t* start, donkey_ll_t* pub)
{
	pub = pub->next;
	if (!pub) {
		pub = start;
	}
	return pub;
}

/**
 * Finds a (new) publisher of which the data should be synchronized with the
 * remote peer.
 *
 * @param	session	session instance
 * @return			returns new publisher to be synchronized or NULL if there
 *                  is nothing to be synchronized.
 */
donkey_ll_t* donkey_session_pub_get(void* session, donkey_f_publish publish, void* owner)
{
	donkey_session_t* self = (donkey_session_t*)session;
	donkey_ll_t* start = self->pub;
	donkey_ll_t* end = self->pub_sync;
	donkey_ll_t** pub = &(self->pub_sync);

	if (!start || !end || !(*pub) || !(self->pub_sync_complete)) {
		return *pub;
	}

	do {
		if (donkey_mmap_is_synced((*pub)->mmap)) {
			if ( publish(owner,DONKEY_T_MMAP,(*pub)->id) ) {
				self->pub_sync_complete = false;
				return *pub;
			}
		}
		*pub = donkey_session_pub_next(start, *pub);

	} while (*pub != end);

	return NULL;
}

/**
 * Marks the current publisher as synchronized
 *
 * @param	session	session instance
 * @return			void
 */
void donkey_session_pub_synced(void* session)
{
	donkey_session_t* self = (donkey_session_t*)session;
	self->pub_sync_complete = true;
	self->pub_sync = donkey_session_pub_next(self->pub, self->pub_sync);
}

/**
 * Adds session information to the data section of STX frame and
 * returns the new offset within the data section.
 *
 * @param	session	session instance
 * @return			new offset within the frame data section
 */
uint8_t* donkey_session_add_info(void* session, uint8_t* p)
{
	donkey_session_t* self = (donkey_session_t*)session;

	// pub/sub 100ms: total size and number of elements
	p = donkey_add_uint16(p, donkey_mmap_size(self->pub_100ms.mmap));
	p = donkey_add_uint16(p, donkey_mmap_nrel(self->pub_100ms.mmap));
	p = donkey_add_uint16(p, donkey_mmap_size(self->sub_100ms.mmap));
	p = donkey_add_uint16(p, donkey_mmap_nrel(self->sub_100ms.mmap));

	// pub/sub 1s: total size and number of elements
	p = donkey_add_uint16(p, donkey_mmap_size(self->pub_1s.mmap));
	p = donkey_add_uint16(p, donkey_mmap_nrel(self->pub_1s.mmap));
	p = donkey_add_uint16(p, donkey_mmap_size(self->sub_1s.mmap));
	p = donkey_add_uint16(p, donkey_mmap_nrel(self->sub_1s.mmap));

	// pub/sub: total size and number of elements
	p = donkey_add_uint32(p, self->pub_size);
	p = donkey_add_uint16(p, self->pub_nrel);
	p = donkey_add_uint32(p, self->sub_size);
	p = donkey_add_uint16(p, self->sub_nrel);
	return p;
}

/**
 * Adds session checksums to the data section of STX frame and
 * returns the new offset within the data section.
 *
 * @param	session	session instance
 * @return			new offset within the frame data section
 */
uint8_t* donkey_session_add_crc(void* session, uint8_t* p)
{
	donkey_crc_t crc, crc2;
	donkey_ll_t* it;
	donkey_session_t* self = (donkey_session_t*)session;

	// pub/sub 100ms: crc(id's) and crc(sizes)
	crc = donkey_mmap_crc(self->pub_100ms.mmap);
	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);
	crc = donkey_mmap_crc(self->sub_100ms.mmap);
	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);

	// pub/sub 1s: crc(id's) and crc(sizes)
	crc = donkey_mmap_crc(self->pub_1s.mmap);
	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);
	crc = donkey_mmap_crc(self->sub_1s.mmap);
	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);

	// pub: crc(id's) and crc(sizes)
	for (crc.id=0, crc.size=0, it=self->pub; it; it=it->next) {
		crc2 = donkey_mmap_crc(it->mmap);
		crc.id = crc16_add( (char*)&(crc2.id), sizeof(crc2.id), crc.id);
		crc.size = crc16_add( (char*)&(crc2.size), sizeof(crc2.size), crc.size);
	}
	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);

	// sub: crc(id's) and crc(sizes)
	for (crc.id=0, crc.size=0, it=self->sub; it; it=it->next) {
		crc2 = donkey_mmap_crc(it->mmap);
		crc.id = crc16_add( (char*)&(crc2.id), sizeof(crc2.id), crc.id);
		crc.size = crc16_add( (char*)&(crc2.size), sizeof(crc2.size), crc.size);
	}

	p = donkey_add_uint16(p, crc.id);
	p = donkey_add_uint16(p, crc.size);
	return p;
}

/**
 * Returns true when the given frame contains valid session information
 *
 * @param	session	session instance
 * @param	p		pointer to encoded session information
 * @return			true when valid
 */
bool donkey_session_info_is_valid(void* session, uint8_t* p)
{
	uint16_t his, mine;
	uint32_t size;
	donkey_session_t* self = (donkey_session_t*)session;

	// pub/sub 100ms: size and nrel
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_size(self->sub_100ms.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_nrel(self->sub_100ms.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_size(self->pub_100ms.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_nrel(self->pub_100ms.mmap);
	if (his != mine) {
		return false;
	}

	// pub/sub 1s: size and nrel
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_size(self->sub_1s.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_nrel(self->sub_1s.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_size(self->pub_1s.mmap);
	if (his != mine) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	mine = donkey_mmap_nrel(self->pub_1s.mmap);
	if (his != mine) {
		return false;
	}

	// pub/sub: size and nrel
	p = donkey_get_uint32(p, &size);
	if (size != self->sub_size) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	if (his != self->sub_nrel) {
		return false;
	}
	p = donkey_get_uint32(p, &size);
	if (size != self->pub_size) {
		return false;
	}
	p = donkey_get_uint16(p, &his);
	if (his != self->pub_nrel) {
		return false;
	}
	return true;
}

/**
 * Returns true when the given frame contains valid session checksums
 *
 * @param	session	session instance
 * @param	p		pointer to encoded session checksums
 * @return			true when valid
 */
bool donkey_session_crc_is_valid(void* session, uint8_t* p)
{
	donkey_crc_t crc, crc2;
	uint16_t val;
	donkey_ll_t* it;
	donkey_session_t* self = (donkey_session_t*)session;

	// check pub/sub 100ms: id and size
	crc = donkey_mmap_crc(self->sub_100ms.mmap);
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}
	crc = donkey_mmap_crc(self->pub_100ms.mmap);
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}

	// check pub/sub 1s: id and size
	crc = donkey_mmap_crc(self->sub_1s.mmap);
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}
	crc = donkey_mmap_crc(self->pub_1s.mmap);
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}

	// calc sub crc's and check with received pub
	for (crc.id=0, crc.size=0, it=self->sub; it; it=it->next) {
		crc2 = donkey_mmap_crc(it->mmap);
		crc.id = crc16_add( (char*)&(crc2.id), sizeof(crc2.id), crc.id);
		crc.size = crc16_add( (char*)&(crc2.size), sizeof(crc2.size), crc.size);
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}

	// calc pub crc's and check with received sub
	for (crc.id=0, crc.size=0, it=self->pub; it; it=it->next) {
		crc2 = donkey_mmap_crc(it->mmap);
		crc.id = crc16_add( (char*)&(crc2.id), sizeof(crc2.id), crc.id);
		crc.size = crc16_add( (char*)&(crc2.size), sizeof(crc2.size), crc.size);
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.id) {
		return false;
	}
	p = donkey_get_uint16(p, &val);
	if (val != crc.size) {
		return false;
	}
	return true;
}

