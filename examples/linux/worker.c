#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>

#include "worker.h"

#define USAGE(out,name) \
        fprintf(out, "Usage: %s [-t m|s|p] [-d device]\n", name);

        
#define TIMEOUT 100 // use a 100ms refresh rate        
        
typedef struct {
    uint32_t elapsed;
    bool log;
    char tag;
} worker_t;

static bool
publish(donkey_type_t type, uint16_t id, size_t size, void* data, void* user);

static void
consume(donkey_type_t type, uint16_t id, size_t size, void *data, void *user);

static void
create_pubsub(worker_t *self, void *donkey, uint16_t id, size_t size, 
              donkey_type_t type);

static void
info_pubsub(worker_t *self, void* donkey, uint16_t id);

static void
print_state(worker_t *self, void *donkey);

int
worker(char tag, FD fd, int session, uint32_t duration, bool log, 
       pubsub_t pubsub[])
{
    bool err=false;
	void *donkey;
	donkey_dev_t device = { read, write, fd };
	worker_t self = { 0, log, tag };
    pubsub_t *p;
    
	donkey = donkey_create(session, device);
	if (donkey==NULL) {
        fprintf(stderr, "%s %i: (%c) %s\n", __FILE__, __LINE__, self.tag, 
                strerror(donkey_errno));
		return -1;
	}
    
    // create publications and subscriptions
    for (p=pubsub; p->id>0; p++)
	    create_pubsub(&self, donkey, p->id, p->size, p->type);
        
    // display publication and subscription information
    for (p=pubsub; p->id>0; p++)
        info_pubsub(&self, donkey, p->id);
    
	// start communication with other peer
	while (self.elapsed < duration) {
		if (donkey_sync(donkey, TIMEOUT)) {
			if (donkey_errno != ENODATA) {
                fprintf(stderr, "%s %i: (%c) %i:%s\n", __FILE__, __LINE__,
                        self.tag, donkey_errno, strerror(donkey_errno));
                err=true;
				goto worker_exit;
			}
		}
		if (self.log) print_state(&self, donkey);
		usleep(TIMEOUT * 1000);
		self.elapsed += TIMEOUT;
	}

worker_exit:
	if (donkey_destroy(donkey)) {
        fprintf(stderr, "%s %i: (%c) %s\n", __FILE__, __LINE__, 
                self.tag, strerror(donkey_errno));
        err=true;
	}
    return (err?-1:0);
}

static bool
publish(donkey_type_t type, uint16_t id, size_t size, void *data, void *user)
{
    bool changed=true;
    static char c='a';
    worker_t *self = (worker_t*)user;
    
    //if (self->log)
    printf("(%c) %i PUB(id=%X), type=%i, size=%lu\n", self->tag,
               self->elapsed, id, type, (unsigned long)size);

    memset(data, c, size);
    if (++c > 'Z') c = 'a';
        
	return changed;
}

static void
consume(donkey_type_t type, uint16_t id, size_t size, void *data, void *user)
{
	int i;
	char *c = (char*)data;
    worker_t *self = (worker_t*)user;

    //if (self->log) {
	    printf("(%c) %i SUB(id=%X), type=%i, size=%lu, data=", self->tag,
               self->elapsed, id, type, (unsigned long)size);
	    for (i=0; i < size; i++) printf("%c", c[i]);
	    printf("\n");
    //}
}

static void
create_pubsub(worker_t *self, void *donkey, uint16_t id, size_t size, 
              donkey_type_t type)
{
	int ret;

	ret = donkey_publication(donkey, id, size, publish, self, type);
	if (ret != 0) {
        fprintf(stderr, "(%c): failed to create publication(%i,%lu,%i): %i\n",
		    self->tag, id, size, type, donkey_errno);
		donkey_destroy(donkey);
		exit(EXIT_FAILURE);
	}

	ret = donkey_subscribe(donkey, id, size, consume, self, type);
	if (ret != 0) {
        fprintf(stderr, "(%c): failed to subscribe(%i,%lu,%i): %i\n",
		    self->tag, id, size, type, donkey_errno);
		donkey_destroy(donkey);
		exit(EXIT_FAILURE);
	}
}

static void
info_pubsub(worker_t *self, void *donkey, uint16_t id)
{
	donkey_info_t info;

	info = donkey_pub_info(donkey, id);
	printf("(%c): pub(%i) best=%i,%lu worst=%i,%lu raw=%lu\n", self->tag, id,
		info.t_best, info.bps_best, info.t_worst, info.bps_worst, info.bps_raw);

	info = donkey_sub_info(donkey, id);
	printf("(%c): sub(%i) best=%i,%lu worst=%i,%lu raw=%lu\n", self->tag, id,
		info.t_best, info.bps_best, info.t_worst, info.bps_worst, info.bps_raw);
}

static void
print_state(worker_t *self, void *donkey)
{
	donkey_state_t state = donkey_get_state(donkey);
	switch (state) {
	case DONKEY_ST_SYNC:
		printf("=%c= SYNC    %i\n", self->tag, self->elapsed/TIMEOUT);
		break;
	case DONKEY_ST_START:
		printf("=%c= START   %i\n", self->tag, self->elapsed/TIMEOUT);
		break;
	case DONKEY_ST_ONLINE:
        printf("=%c= ONLINE  %i\n", self->tag, self->elapsed/TIMEOUT);
		break;
	case DONKEY_ST_OFFLINE:
        printf("=%c= OFFLINE %i\n", self->tag, self->elapsed/TIMEOUT);
		break;
	case DONKEY_ST_UNDEFINED:
	default:
		printf("=%c= ????    %i\n", self->tag, self->elapsed/TIMEOUT);
		break;
	}
}



