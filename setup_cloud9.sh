#!/bin/bash
set -e

tmp=`mktemp -d -p $HOME`
version=1.8.17

mkdir -p $tmp
cd $tmp

wget https://waf.io/waf-$version.tar.bz2
tar jxvf waf-$version.tar.bz2
cd $tmp/waf-$version

python3 waf-light --make-waf --prefix=$HOME --tools=eclipse

mkdir -p ~/bin
cp waf ~/bin

cd ~
rm -rf $tmp
