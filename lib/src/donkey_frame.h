#ifndef DONKEY_FRAME_H
#define DONKEY_FRAME_H

#include "config.h"

#if HAVE_STDDEF_H
#include <stddef.h>
#endif

#if HAVE_STDBOOL_H 
#include <stdint.h>
#endif

#if HAVE_STDINT_H
#include <stdbool.h>
#endif

#include "crc16_ccitt.h"

// frame identifiers
#define DONKEY_FID_SYN		"#SYN"			// 0x2353594e
#define DONKEY_FID_ACK		"#ACK"			// 0x2341434b
#define DONKEY_FID_STX		"#STX"			// 0x23535458
#define DONKEY_FID_NAK		"#NAK"			// 0x234e414b
#define DONKEY_FID_BIN		"#BIN"			// 0x2342494e
#define DONKEY_FID_RST		"#RST"			// 0x23525354

// frame token
#define DONKEY_FRAME_TOKEN	"@$"			// 0x4024

// size in bytes of frame fields
#define DONKEY_FID			4
#define DONKEY_SID			2
#define DONKEY_TOK			2
#define DONKEY_LEN			2
#define DONKEY_CRC			2
#define DONKEY_FRAME_HEADER	(DONKEY_FID + DONKEY_SID + DONKEY_TOK + DONKEY_LEN)
#define DONKEY_FRAME_EMPTY	(DONKEY_FRAME_HEADER + DONKEY_CRC)

// start position of frame fields
#define DONKEY_O_FID		0
#define DONKEY_O_SID		(DONKEY_FID)
#define DONKEY_O_TOK		(DONKEY_FID + DONKEY_SID)
#define DONKEY_O_LEN		(DONKEY_FID + DONKEY_SID + DONKEY_TOK)
#define DONKEY_O_DATA		(DONKEY_FID + DONKEY_SID + DONKEY_TOK + DONKEY_LEN)

size_t donkey_frame_size(uint16_t len);
size_t donkey_frame_syn(uint8_t* frame, uint16_t sid);
size_t donkey_frame_stx(uint8_t* frame, uint16_t sid, uint16_t len, uint8_t* data);
size_t donkey_frame_ack_syn(uint8_t* frame, uint16_t sid);
size_t donkey_frame_ack_stx(uint8_t* frame, uint16_t sid);
size_t donkey_frame_nak(uint8_t* frame, uint16_t sid);
size_t donkey_frame_bin(uint8_t* frame, uint16_t sid, uint16_t len, uint8_t* data);
size_t donkey_frame_rst(uint8_t* frame, uint16_t sid);

uint16_t donkey_frame_get_len(uint8_t* frame);
uint8_t* donkey_frame_get_data(uint8_t* frame);

bool donkey_is_frame_header(uint8_t* frame);
bool donkey_is_frame(uint8_t* frame);
bool donkey_frame_is_syn(uint8_t* frame);
bool donkey_frame_is_stx(uint8_t* frame);
bool donkey_frame_is_ack(uint8_t* frame);
bool donkey_frame_is_ack_syn(uint8_t* frame);
bool donkey_frame_is_ack_stx(uint8_t* frame);
bool donkey_frame_is_nak(uint8_t* frame);
bool donkey_frame_is_bin(uint8_t* frame);
bool donkey_frame_is_rst(uint8_t* frame);

#endif // DONKEY_FRAME_H

