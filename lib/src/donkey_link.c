#include <stdint.h>
#include <string.h>
#include "donkey_link.h"
#include "donkey_frame.h"
#include "donkey_mmap.h"
#include "donkey.h"

#define DONKEY_FRAME_SIZE (DONKEY_FRAME_EMPTY + DONKEY_DATA_SIZE)

/**
 * Defines buffer for sending and receiving frames
 */
typedef struct {
	uint8_t				start[DONKEY_FRAME_SIZE];
	uint8_t*			end;
	uint8_t*			curr;
	size_t				size;
} donkey_frame_t;

/**
 * Definition of a link instance
 */
typedef struct {
	uint16_t			id;
	void*				session;
	uint32_t			ticks;
	uint32_t			t_elapsed;
	donkey_state_t		state;
	bool				reset;

	void*				owner;
	donkey_f_publish 	publish;
	donkey_f_consume 	consume;

	donkey_dev_t		device;
	donkey_frame_t		tx;
	donkey_frame_t		rx;
	uint32_t			rx_timestamp;
	uint32_t			rx_timeout;
} donkey_link_t;

/**
 * Creates a new link layer instance
 *
 * @param	init	initialization string containing device and device settings to use
 * @param	id		link identifier, should match with that of the remote peer
 * @param	publish	function to be called when its time to publish new data
 * @param	consume	function to be called when new subscription data is available
 * @param	owner	reference to the owner creating this link
 * @return			returns link instance or NULL in case of failure
 */
void* donkey_link_create(uint16_t id, donkey_dev_t device, void* session,
			donkey_f_publish publish, donkey_f_consume consume, void* owner)
{
	donkey_link_t* self;

	self = calloc(1, sizeof(donkey_link_t));
	if (!self) {
		DONKEY_ERROR(NULL, DONKEY_ENOMEM, "failed to create link");
	}

	self->id = id;
	self->device = device;
	self->session = session;
	self->owner = owner;
	self->publish = publish;
	self->consume = consume;
	self->ticks = 0;
	self->t_elapsed = 0;
	self->state = DONKEY_ST_SYNC;
	self->reset = false;
	self->tx.size = DONKEY_FRAME_SIZE;
	self->tx.end = self->tx.start + self->tx.size;
	self->tx.curr = self->tx.start;
	self->rx.size = DONKEY_FRAME_SIZE;
	self->rx.end = self->rx.start + self->rx.size;
	self->rx.curr = self->rx.start;
	self->rx_timestamp = 0;
	self->rx_timeout = DONKEY_TIMEOUT;
	return self;
}

/**
 * Destroys a link layer instance
 *
 * @param	link	link instance
 * @return			returns non-zero on failure
 */
int donkey_link_destroy(void* link)
{
	donkey_link_t* self = (donkey_link_t*)link;
	free(self);
	return 0;
}

/**
 * Transmits a frame
 *
 * @param	self	link instance
 * @param	frame	encoded frame to be transmitted
 * @param	size	frame size in bytes
 * @return			returns non-zero on failure
 */
static int donkey_link_tx(donkey_link_t* self, uint8_t* frame, size_t size)
{
	int res;

	res = self->device.write(self->device.fd, frame, size);
	if (res != (int)size) {
		DONKEY_ERROR(-1, DONKEY_ECOMM, "failed to send");
	}
	return 0;
}

/**
 * Transmits a synchronization frame (SYN)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_syn(donkey_link_t* self)
{
	size_t size = donkey_frame_syn(self->tx.start, self->id);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits a 'start transmit' frame (STX)
 * The frame contains information regarding the size and number of
 * elements of all published (see protocol frame description)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_stx(donkey_link_t* self)
{
	size_t size;
	uint8_t* data = donkey_frame_get_data(self->tx.start);
	uint8_t* p = data;

	p = donkey_session_add_info(self->session, p);
	p = donkey_session_add_crc(self->session, p);

	if ((p-data) != DONKEY_D_SESSION) {
		DONKEY_EXCEPTION(-1, DONKEY_EFAULT, "start transmit link error");
	}

	size = donkey_frame_stx(self->tx.start, self->id, DONKEY_D_SESSION, data);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits a synchronization acknowledge frame (ACK:SYN)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_ack_syn(donkey_link_t* self)
{
	size_t size = donkey_frame_ack_syn(self->tx.start, self->id);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits a 'start transmit' acknowledge frame (ACK:STX)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_ack_stx(donkey_link_t* self)
{
	size_t size = donkey_frame_ack_stx(self->tx.start, self->id);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits a not acknowledged frame (NAK)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_nak(donkey_link_t* self)
{
	size_t size = donkey_frame_nak(self->tx.start, self->id);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits a data frame (BIN)
 *
 * @param	self	link instance
 * @param	len		lenght in bytes of the frame data
 * @param	data	frame data
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_bin(donkey_link_t* self, uint16_t len, uint8_t* data)
{
	size_t size = donkey_frame_bin(self->tx.start, self->id, len, data);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Transmits link reset frame (RST)
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_tx_rst(donkey_link_t* self)
{
	size_t size = donkey_frame_rst(self->tx.start, self->id);
	return donkey_link_tx(self, self->tx.start, size);
}

/**
 * Clears the content of the frame receive buffer
 *
 * @param	self	link instance
 * @return			void
 */
static void donkey_link_rx_flush(donkey_link_t* self)
{
	memset(self->rx.start, 0, self->rx.size);
	self->rx.curr = self->rx.start;
}

/**
 * Reads and stores the requested bytes in the frame receive buffer
 *
 * @param	self	link instance
 * @param	size	number of bytes to be read
 * @return			return non-zero in case of failure
 */
static int donkey_link_rx_read(donkey_link_t* self, size_t size)
{
	int res;

	if (!size) {
		return 0;
	}

	if ( (self->rx.curr + size) > self->rx.end) {
		donkey_link_rx_flush(self);
		DONKEY_EXCEPTION(-1, DONKEY_EFAULT, "link read error");
	}

	res = self->device.read(self->device.fd, self->rx.curr, size);
	if (res > 0) {
		self->rx.curr += size;
		self->rx_timestamp = self->ticks;
	}

	if (res != size) {
		donkey_errno = DONKEY_ENODATA;
		return 1;
	}
	return 0;
}

/**
 * Finds the start of a frame in the receive buffer and moves it
 * to the start of that buffer.
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_rx_find(donkey_link_t* self)
{
	uint8_t* p;
	uint16_t len;

	// find start of frame token
	for (p=(self->rx.start+1); p != self->rx.end && *p != '#'; p++) {
		/* noop */;
	}

	if (p == self->rx.end) { // no start token found; flush all data
		self->rx.curr = self->rx.start;
		memset(self->rx.start, 0, self->rx.size);
	}
	else if (*p == '#') { // move start of frame to start of rx buffer
		len = self->rx.end - p;
		memmove(self->rx.start, p, len);
		self->rx.curr = p + 1;
	}
	else { // internal error; flush all data
		donkey_link_rx_flush(self);
		DONKEY_EXCEPTION(-1, DONKEY_EFAULT, "link receive error");
	}
	return 0;
}

/**
 * Receive and store a new frame in the receive buffer.
 *
 * @param	self	link instance
 * @return			0=complete, 1=incomplete, -1=error
 */
static int donkey_link_rx(donkey_link_t* self)
{
	int res;
	uint16_t len;
	uint8_t* frame = self->rx.start;

	while ( !donkey_is_frame(self->rx.start) ) {
		// get minimum frame size
		len = DONKEY_FRAME_EMPTY - (self->rx.curr - self->rx.start);
		res = donkey_link_rx_read(self, len);
		if (res != 0) {
			return res;
		}

		// check if this is a valid frame header
		if ( donkey_is_frame_header(self->rx.start) ) {
			len = donkey_frame_get_len(self->rx.start);
			res = donkey_link_rx_read(self, len);
			if (res != 0) {
				return res;
			}

			// frame should be complete now, check crc
			if ( !donkey_is_frame(self->rx.start) ) {
				res = donkey_link_rx_find(self); // invalid frame; find start of next frame
				if (res != 0) {
					return res;
				}
			}
		}
		else {
			res = donkey_link_rx_find(self);
			if (res != 0) {
				return res;
			}
		}
	}
	return 0;
}

/**
 * Returns true in case of a valid 'start transmit' frame containing
 * matching publication and subscription information with that of the
 * local peer.
 * matches publication size and number of elements from other peer with
 * subscription size and number of element on this peer and vice versa.
 *
 * @param	self	link instance
 * @param	frame	encoded 'start transmit' frame
 * @return			returns true when valid
 */
static bool donkey_link_is_valid_stx(donkey_link_t* self, uint8_t* frame)
{
	uint16_t len = donkey_frame_get_len(frame);
	uint8_t* p = donkey_frame_get_data(frame);

	if (len != DONKEY_D_SESSION) {
		return false;
	}

	if ( !donkey_session_info_is_valid(self->session, p) ) {
		return false;
	}

	if ( !donkey_session_crc_is_valid(self->session, p + DONKEY_D_SESSION_INFO) ) {
		return false;
	}

	return true;
}

/**
 * Consumes published 'normal' mmap from remote peer
 *
 * @param	self	link instance
 * @param	data	current offset in the data section of a frame
 * @return			return new data offset or NULL in case of failure
 */
static uint8_t* donkey_link_consume_mmap(donkey_link_t* self, uint8_t* data)
{
	bool segments=false, last=false;
	uint16_t id, len;
	uint32_t segment = 0;
	uint8_t* p = data;
	donkey_sync_t sync;
	donkey_ll_t* sub;

	// get IDx
	memcpy(&id, p, DONKEY_D_ID);
	id = ntohs(id);
	id &= DONKEY_M_ID;
	p += DONKEY_D_ID;

	// get LENx
	memcpy(&len, p, DONKEY_D_LEN);
	len = ntohs(len);
	if (len & DONKEY_B_SEG) {
		segments = true;
	}
	if (len & DONKEY_B_LAST) {
		last = true;
	}
	len &= DONKEY_M_LEN;
	if (!len) {
		DONKEY_WARNING(NULL, DONKEY_EBADMSG, "corrupt message length received");
	}
	p += DONKEY_D_LEN;

	// get SEG
	if (segments) {
		memcpy(&segment, p, DONKEY_D_SEG);
		segment = ntohl(segment);
		p += DONKEY_D_SEG;
	}

	// find matching subscription
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP);
	for ( ; sub && sub->id!=id; sub=sub->next) {
		/* noop */;
	}
	if (!sub) {
		DONKEY_WARNING(NULL, DONKEY_EBADMSG, "subscription not found");
	}

	// write segment to subcribers mmap
	sync = donkey_mmap_sync(sub->mmap, len);
	if (sync.segment != segment) {
		DONKEY_WARNING(NULL, DONKEY_EBADMSG, "failed to synchronize segment");
	}
	memcpy(sync.data, p, sync.size);
	p += len;

	// notify subscriber when mmap has been synchronized
	if (donkey_mmap_is_synced(sub->mmap)) {
		if (!last) {
			DONKEY_WARNING(NULL, DONKEY_EBADMSG, "failed to synchronize last segment");
		}
		self->consume(self->owner, DONKEY_T_MMAP, sub->id);
	}
	else if (last) {
		DONKEY_WARNING(NULL, DONKEY_EBADMSG, "received too much segments");
	}
	return p;
}

/**
 * Consumes published data from remote peer
 *
 * @param	self	link instance
 * @param	frame	encoded frame containing published data
 * @return			return non-zero on failure
 */
static int donkey_link_consume(donkey_link_t* self, uint8_t* frame)
{
	uint16_t fr, len = donkey_frame_get_len(frame);
	uint8_t* start = donkey_frame_get_data(frame);
	uint8_t* end = start + len;
	uint8_t* p = start;
	donkey_sync_t sync;
	donkey_ll_t* sub;

	if (len > DONKEY_DATA_SIZE) {
		DONKEY_WARNING(-1, DONKEY_EBADMSG, "message received is too large");
	}

	// get 100ms mmap data and publish when synced
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP_100MS);
	sync = donkey_mmap_sync(sub->mmap, DONKEY_MMAP_100MS);
	memcpy(sync.data, p, sync.size);
	p += sync.size;
	if (donkey_mmap_is_synced(sub->mmap)) {
		self->consume(self->owner, DONKEY_T_MMAP_100MS, DONKEY_ID_NONE);
	}

	// get current 1s mmap (sub)frame and publish when synced
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP_1S);
	sync = donkey_mmap_sync(sub->mmap, DONKEY_D_1S); // use frame size
	memcpy(&fr, p, DONKEY_D_FR);
	p += DONKEY_D_FR;
	fr = ntohs(fr);
	if (sync.segment != fr) {
		DONKEY_WARNING(-1, DONKEY_EBADMSG, "received illegal (sub)frame");
	}

	memcpy(sync.data, p, sync.size);
	p += sync.size;
	if (donkey_mmap_is_synced(sub->mmap)) {
		self->consume(self->owner, DONKEY_T_MMAP_1S, DONKEY_ID_NONE);
	}

	// get and publish 'normal' mmap data
	while (p != end) {
		p=donkey_link_consume_mmap(self,p);
		if (!p) {
			return -1;
		}
		if (p > end) {
			return -1;
		}
	};
	return 0;
}

/**
 * Adds a (segment) of a mmap of which the state needs to be synchronized with
 * the remote peer, to the data section of a transmit frame.
 *
 * @param	self	link instance
 * @param	data	start offset of the data section of a frame
 * @param	p		current offset within the data section of a frame
 * @return			returns new data offset
 */
static uint8_t* donkey_link_add_mmap(donkey_link_t* self, uint8_t* data, size_t available)
{
	uint16_t id, len;
	donkey_ll_t* pub;
	size_t size;
	donkey_sync_t sync;
	uint8_t* p = data;
	bool segments = false;
	uint32_t segment;

	// find next publisher to synchronize
	pub = donkey_session_pub_get(self->session, self->publish, self->owner);
	if (!pub) {
		return data;
	}
	size = donkey_mmap_size(pub->mmap);
	id = pub->id;

	// calculate sizeof and get data segment
	if ((DONKEY_D_ID + DONKEY_D_LEN + size) <= available) { // entire mmap in one go
		len = 0;
	}
	else { // fragmented
		if ((DONKEY_D_ID + DONKEY_D_LEN + DONKEY_D_SEG + 1) > available) {
			return data; // no room to send any data
		}
		segments = true;
		size = available - DONKEY_D_ID - DONKEY_D_LEN - DONKEY_D_SEG;
		len = DONKEY_B_SEG;
	}
	sync = donkey_mmap_sync(pub->mmap, size);
	len += (uint16_t)sync.size;

	// encode and store data(segment) in the frame
	if (donkey_mmap_is_synced(pub->mmap)) {
		len |= DONKEY_B_LAST;
		donkey_session_pub_synced(self->session);
	}

	id = htons(id);
	memcpy(p, &id, DONKEY_D_ID);
	p += DONKEY_D_ID;
	len = htons(len);
	memcpy(p, &len, DONKEY_D_LEN);
	p += DONKEY_D_LEN;
	if (segments) {
		segment = htonl(sync.segment);
		memcpy(p, &segment, DONKEY_D_SEG);
		p += DONKEY_D_SEG;
	}
	memcpy(p, sync.data, sync.size);
	p += sync.size;
	return p;
}

/**
 * Publishes data when the link has been established
 *
 * @param	self	link instance
 * @return			returns non-zero on failure
 */
static int donkey_link_publish(donkey_link_t* self)
{
	uint8_t* data = donkey_frame_get_data(self->tx.start);
	uint8_t *q, *p = data;
	uint16_t fr;
	size_t size;
	donkey_sync_t sync;
	donkey_ll_t* pub;

	// publish 100ms mmap
	pub = donkey_session_pub(self->session, DONKEY_T_MMAP_100MS);
	if (donkey_mmap_is_synced(pub->mmap)) {
		self->publish(self->owner, DONKEY_T_MMAP_100MS, DONKEY_ID_NONE);
	}
	sync = donkey_mmap_sync(pub->mmap, DONKEY_MMAP_100MS);
	memcpy(p, sync.data, sync.size);
	p += sync.size;

	// publish 1s mmap
	pub = donkey_session_pub(self->session, DONKEY_T_MMAP_1S);
	if (donkey_mmap_is_synced(pub->mmap)) {
		self->publish(self->owner, DONKEY_T_MMAP_1S, DONKEY_ID_NONE);
	}
	sync = donkey_mmap_sync(pub->mmap, DONKEY_D_1S); // use frame size
	fr = (uint16_t)sync.segment;
	fr = htons(fr);
	memcpy(p, &fr, DONKEY_D_FR);
	p += DONKEY_D_FR;
	memcpy(p, sync.data, sync.size);
	p += sync.size;

	// publish mmap
	for (q=NULL; q!=p; p=q) {
		size = DONKEY_DATA_SIZE - (p-data);
		q = donkey_link_add_mmap(self, p, size);
	}

	size = p - data;
	if (size > DONKEY_DATA_SIZE) {
		DONKEY_EXCEPTION(-1, DONKEY_EFAULT, "publish internal link error");
	}
	return donkey_link_tx_bin(self, size, data);
}

/**
 * Changes the current link state, resets synchronisation sequences
 * of memory mapped data.
 *
 * @param	self	link instance
 * @param	state	new state
 * @return			void
 */
static void donkey_change_state(donkey_link_t* self, donkey_state_t state)
{
	self->state = state;
	donkey_session_reset(self->session);
}

/**
 * Behavior when there is no link with the remote peer.
 *
 * @param	self	link instance
 * @param	frame	received data frame
 * @param	cnt		frame count [0..] in current system tick
 * @return			returns non-zero on failure
 */
static int donkey_link_sync(donkey_link_t* self, uint8_t* frame, int cnt, bool* ack_syn)
{
	int res = 0;

	if (cnt == 0) {
		res = donkey_link_tx_syn(self);
		if (res != 0) {
			return -1;
		}
	}

	if (frame) {
		if (donkey_frame_is_syn(frame)) {
			if (*ack_syn) {
				res = donkey_link_tx_ack_syn(self);
				*ack_syn = false;
			}
		}
		else if (donkey_frame_is_ack_syn(frame)) {
			res = donkey_link_tx_stx(self);
			if (res == 0) {
				donkey_change_state(self, DONKEY_ST_START);
			}
		}
	}
	return res;
}

/**
 * Behavior when establishing a new link with the remote peer.
 *
 * @param	self	link instance
 * @param	frame	received data frame
 * @param	cnt		frame count [0..] in the current system tick
 * @param	ack_syn defines if a ACK(SYN) has been send in the current system tick
 * @return			returns non-zero on failure
 */
static int donkey_link_start(donkey_link_t* self, uint8_t* frame, int cnt, bool* ack_syn)
{
	int res = 0;

	if (frame) {
		if (donkey_frame_is_syn(frame)) {
			if (*ack_syn) {
				res = donkey_link_tx_ack_syn(self);
				*ack_syn = false;
			}
		}
		else if (donkey_frame_is_ack_syn(frame)) {
			// discard frame
		}
		else if (donkey_frame_is_stx(frame)) {
			if (donkey_link_is_valid_stx(self, frame)) {
				res = donkey_link_tx_ack_stx(self);
			}
			else {
				donkey_link_tx_nak(self);
				donkey_change_state(self, DONKEY_ST_OFFLINE);
			}
		}
		else if (donkey_frame_is_ack_stx(frame)) {
			donkey_change_state(self, DONKEY_ST_ONLINE);
		}
		else if (donkey_frame_is_nak(frame)) {
			donkey_change_state(self, DONKEY_ST_OFFLINE);
		}
		else {
			donkey_change_state(self, DONKEY_ST_SYNC);
		}
	}
	return res;
}

/**
 * Behavior when link has been established with remote peer; consumes received
 * binary frames (containing subscription data) and publishes (sends) binary
 * frame to remote peer.
 *
 * @param	self	link instance
 * @param	frame	received data frame
 * @param	cnt		frame count [0..] in current system tick
 * @return			returns non-zero on failure
 */
static int donkey_link_online(donkey_link_t* self, uint8_t* frame, int cnt)
{
	int res = 0;

	if (frame) {
		if (donkey_frame_is_bin(frame)) {
			res = donkey_link_consume(self, frame);
			if (res != 0) {
				donkey_link_tx_nak(self);
				donkey_change_state(self, DONKEY_ST_SYNC);
				return res;
			}
		}
		else {
			donkey_change_state(self, DONKEY_ST_SYNC);
			return 0;
		}
	}

	if (cnt == 0) {
		res = donkey_link_publish(self);
		if (res != 0) {
			donkey_change_state(self, DONKEY_ST_SYNC);
		}
	}
	return res;
}

/**
 * Behavior when link could not be established with remote peer (e.g. mismatch detected
 * in publication data between both peers.
 *
 * @param	self	link instance
 * @param	frame	received data frame
 * @param	cnt		frame count [0..] in the current system tick
 * @param	ack_syn defines if a ACK(SYN) has been send in the current system tick
 * @return			returns non-zero on failure
 */
static int donkey_link_offline(donkey_link_t* self, uint8_t* frame, int cnt, bool* ack_syn)
{
	int res=0;

	if (cnt == 0) {
		res = donkey_link_tx_syn(self);
		if (res != 0) {
			return -1;
		}
	}

	if (frame) {
		if (donkey_frame_is_syn(frame)) {
			if (*ack_syn) {
				res = donkey_link_tx_ack_syn(self);
				*ack_syn = false;
			}
		}
		else if (donkey_frame_is_stx(frame)) {
			donkey_link_tx_nak(self);
		}
		else if (donkey_frame_is_rst(frame)) {
			donkey_change_state(self, DONKEY_ST_SYNC);
		}
	}
	return res;
}

/**
 * Establishes a data link with the remote peer, when connected data from local
 * publisher will be synchronized with subscribers on the remote peer and vice
 * versa.
 *
 * @param	link		link instance
 * @param	t_elapsed	time elapsed (ms) since last call to sync
 * @return				returns a non-zero on failure
 */
int donkey_link(void* link, uint32_t t_elapsed)
{
	int res, sts, cnt;
	donkey_link_t* self = (donkey_link_t*)link;
	uint8_t* frame;
	bool ack_syn = true;
	uint32_t ticks;

	self->t_elapsed += t_elapsed;
	ticks = self->t_elapsed / 100;
	self->t_elapsed -= ticks * 100;
	self->ticks += ticks;

	if (!ticks) {
		return 0;
	}

	if (self->reset) {
		self->reset = false;
		donkey_link_tx_rst(self);
		donkey_change_state(self, DONKEY_ST_SYNC);
	}

	for (sts=0, cnt=0, res=0; sts==0 && res==0; cnt++, donkey_link_rx_flush(self)) {
		sts = donkey_link_rx(self);
		if (sts < 0) {
			return sts;
		}
		frame = (sts==0) ? self->rx.start : NULL;

		switch(self->state) {
			default:
				self->state = DONKEY_ST_SYNC;
				// fallthrough
			case DONKEY_ST_SYNC:
				res = donkey_link_sync(self, frame, cnt, &ack_syn);
				break;
			case DONKEY_ST_START:
				res = donkey_link_start(self, frame, cnt, &ack_syn);
				break;
			case DONKEY_ST_ONLINE:
				res = donkey_link_online(self, frame, cnt);
				break;
			case DONKEY_ST_OFFLINE:
				res = donkey_link_offline(self, frame, cnt, &ack_syn);
				break;
		}
	}

	if (self->ticks > (self->rx_timestamp + self->rx_timeout)) {
		donkey_change_state(self, DONKEY_ST_SYNC);
	}
	return res;
}

/**
 * Returns the current link state
 *
 * @param	link	link instance
 * @return			returns link state
 */
donkey_state_t donkey_link_get_state(void* link)
{
	donkey_link_t* self = (donkey_link_t*)link;
	return self->state;
}

/**
 * Returns true when data is being received from the remote peer is
 * connected; i.e. is physically connected and alive.
 *
 * @param	link	link instance
 * @return			see description
 */
bool donkey_link_is_connected(void* link)
{
	uint32_t delta;
	donkey_link_t* self = (donkey_link_t*)link;

	delta = self->rx_timestamp - self->ticks;
	if (delta < self->rx_timeout) {
		return true;
	}
	return false;
}

/**
 * Returns true when link layer with the remote peer is online
 *
 * @param	link	link instance
 * @return			returns true when the link state is online
 */
bool donkey_link_is_online(void* link)
{
	donkey_link_t* self = (donkey_link_t*)link;
	return (self->state == DONKEY_ST_ONLINE);
}

/**
 * Returns the link timeout in ticks of 100ms.
 *
 * @param	link	link instance
 * @return			see description
 */
uint32_t donkey_link_get_timeout(void* link)
{
	donkey_link_t* self = (donkey_link_t*)link;
	return self->rx_timeout;
}

/**
 * Sets the link timeout in ticks of 100ms.
 *
 * @param	link	link instance
 * @param	timeout	link timeout
 * @return			void
 */
void donkey_link_set_timeout(void* link, uint32_t timeout)
{
	donkey_link_t* self = (donkey_link_t*)link;
	self->rx_timeout = timeout;
}

/**
 * Resets the current link state to SYNC
 *
 * @param	link	link instance
 * @return			void
 */
void donkey_link_reset(void* link)
{
	donkey_link_t* self = (donkey_link_t*)link;
	self->reset = true;
}

/**
 * Calculates the transfer time, effective and raw transfer rate
 * in bytes per second.
 *
 * @param	size	number of bytes to be transferred
 * @param	slot	number of bytes available per 100ms slot within the
 *                  data section of the frame
 * @param	time	[out] tranfser time in seconds
 * @param	bps	    [out] effective tranfser rate in bytes per second
 * @param	bps_raw [out] raw tranfser rate in bytes per second
 * @return          none
 */
static void donkey_link_calc(size_t size, size_t slot, uint32_t* time, size_t* bps, size_t* bps_raw)
{
	size_t s;
	int n;

	if (slot < (size + DONKEY_D_ID + DONKEY_D_LEN)) { // fragmented
		s = slot - (DONKEY_D_ID + DONKEY_D_LEN + DONKEY_D_SEG); // transfer per 100ms
		n = size/s + ((size%s)?1:0);
		*time = 100 * n;
		*bps = s * 10;
		*bps_raw = slot * 10;
	}
	else { // not fragmented
		*time = 100;
		*bps = size * 10;
		*bps_raw = (size + DONKEY_D_ID + DONKEY_D_LEN) * 10;
	}
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested 'normal' subscription.
 *
 * @param	donkey	donkey instance
 * @param	size	subscribed data size
 * @return			returns subscription information
 */
static donkey_info_t donkey_link_pub_calc(donkey_link_t* self, size_t size)
{
    uint32_t t, time;
    size_t b, r, slot, size_100ms, size_1s;
    donkey_info_t info;
    donkey_ll_t* pub;

	// determine bytes available per 100ms
	pub = donkey_session_pub(self->session, DONKEY_T_MMAP_100MS);
	size_100ms = donkey_mmap_size(pub->mmap);
	pub = donkey_session_pub(self->session, DONKEY_T_MMAP_1S);
	size_1s = donkey_mmap_size(pub->mmap);
	slot = DONKEY_DATA_SIZE - size_100ms - DONKEY_D_FR - size_1s/10;

	// best: only this sub changes
	donkey_link_calc(size, slot, &(info.t_best), &(info.bps_best), &(info.bps_raw));

	// calculate total transfer time and total bps_raw
	pub = donkey_session_pub(self->session, DONKEY_T_MMAP);
	for (time=0; pub; pub=pub->next) {
		donkey_link_calc(donkey_mmap_size(pub->mmap), slot, &t, &b, &r);
		time += t;
    }

	// worst: all sub's change
    info.t_worst = time;
	info.bps_worst = info.bps_best;
	if (time) {
		info.bps_worst = (info.bps_best * info.t_best) / time;
	}
	return info;
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested publisher.
 *
 * @param	donkey	donkey instance
 * @param	id	    publication identifier
 * @param	size	published data size
 * @param	type	publisher type
 * @param	info	pointer to store the information
 * @return			returns non-zero on failure
 */
donkey_info_t donkey_link_pub_info(void* link, uint16_t id, size_t size, donkey_type_t type)
{
    donkey_info_t info;
	donkey_link_t* self = (donkey_link_t*)link;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		info.t_best = info.t_worst = 100;
		info.bps_best = info.bps_worst = size * 10;
		info.bps_raw = (size + DONKEY_D_ID + DONKEY_D_LEN) * 10;
		break;
	case DONKEY_T_MMAP_1S:
		info.t_best = info.t_worst = 1000;
		info.bps_best = info.bps_worst = size;
		info.bps_raw = (size + DONKEY_D_ID + DONKEY_D_LEN);
		break;
	case DONKEY_T_MMAP:
		info = donkey_link_pub_calc(self, size);
		break;
	default:
        info.t_best = info.t_worst = -1;
        info.bps_best = info.bps_worst = info.bps_raw = 0;
		break;
	}
	return info;
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested 'normal' subscription.
 *
 * @param	link	link instance
 * @param	size	subscribed data size
 * @return			returns subscription information
 */
static donkey_info_t donkey_link_sub_calc(donkey_link_t* self, size_t size)
{
    uint32_t t, time;
    size_t b, r, slot, size_100ms, size_1s;
    donkey_info_t info;
    donkey_ll_t* sub;

	// determine bytes available per 100ms
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP_100MS);
	size_100ms = donkey_mmap_size(sub->mmap);
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP_1S);
	size_1s = donkey_mmap_size(sub->mmap);
	slot = DONKEY_DATA_SIZE - size_100ms - DONKEY_D_FR - size_1s/10;

	// best: only this sub changes
    donkey_link_calc(size, slot, &(info.t_best), &(info.bps_best), &(info.bps_raw));

	// calculate total transfer time and total bps_raw
	sub = donkey_session_sub(self->session, DONKEY_T_MMAP);
	for (time=0; sub; sub=sub->next) {
		donkey_link_calc(donkey_mmap_size(sub->mmap), slot, &t, &b, &r);
		time += t;
    }

	// worst: all sub's change
    info.t_worst = time;
	info.bps_worst = info.bps_best;
    if (time) {
		info.bps_worst = (info.bps_best * info.t_best) / time;
	}
	return info;
}

/**
 * Returns information on the synchronization time and transfer rate of the
 * reuested subscription.
 *
 * @param	link	link instance
 * @param	id	    subscription identifier
 * @param	size	subscribed data size
 * @param	type	subscription type
 * @return			returns subscription information
 */
donkey_info_t donkey_link_sub_info(void* link, uint16_t id, size_t size, donkey_type_t type)
{
    donkey_info_t info;
	donkey_link_t* self = (donkey_link_t*)link;

	switch(type) {
	case DONKEY_T_MMAP_100MS:
		info.t_best = info.t_worst = 100;
		info.bps_best = info.bps_worst = size * 10;
		info.bps_raw = (size + DONKEY_D_ID + DONKEY_D_LEN) * 10;
		break;
	case DONKEY_T_MMAP_1S:
		info.t_best = info.t_worst = 1000;
		info.bps_best = info.bps_worst = size;
		info.bps_raw = (size + DONKEY_D_ID + DONKEY_D_LEN);
		break;
	case DONKEY_T_MMAP:
		info = donkey_link_sub_calc(self, size);
		break;
	default:
        info.t_best = info.t_worst = -1;
        info.bps_best = info.bps_worst = info.bps_raw = 0;
		break;
	}
	return info;
}

