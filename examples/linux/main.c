#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <fcntl.h>

#include "worker.h"

#define DESCRIPTION \
    "Usage: %s [-n name] [-i id] [-f fifo] [-d duration] [-v]   \n\
                                                                \n\
    -n name     name of serial device (/dev/ttyS1)              \n\
    -i id       session identifier (42)                         \n\
    -f fifo     test using fifo (/tmp/donkey)                   \n\
    -d          duration in seconds                             \n\
    \n"

#define USAGE(out,name) fprintf(out,DESCRIPTION,name);

static pubsub_t pubsub1[] = {
    { 1,  128, DONKEY_T_MMAP_100MS },
    { 2,  124, DONKEY_T_MMAP_100MS },
    { 3,  646, DONKEY_T_MMAP_1S },
    { 4,  646, DONKEY_T_MMAP_1S },
    { 5,  646, DONKEY_T_MMAP_1S },
    { 6,  646, DONKEY_T_MMAP_1S },
    { 7,  572, DONKEY_T_MMAP },
    { 8, 4096, DONKEY_T_MMAP },
    { 0,    0, DONKEY_T_MMAP}
};

static pubsub_t pubsub2[] = {
    { 1,  256, DONKEY_T_MMAP_100MS },
    { 2, 2596, DONKEY_T_MMAP_1S },
    { 3, 1024, DONKEY_T_MMAP },
    { 0,    0, DONKEY_T_MMAP }
};

int
main(int argc, char *argv[])
{
    bool verbose=false;
    int opt, ret;
    char *name="/dev/ttyS1";
    char *fifo=NULL;
	int id = 42;
	int duration = 30;
    int fd;
    pubsub_t *pubsub = pubsub1;

    while ((opt = getopt(argc, argv, "d:i:n:f:vh")) != -1) {
        switch (opt) {
        case 'd':
            duration = atoi(optarg);
            break;
        case 'i':
            id = atoi(optarg);
            break;
        case 'n':
            name = optarg;
            break;
        case 'f':
            fifo = optarg;
            break;
        case 'v':
            verbose = true;
            break;
        case 'h':
            USAGE(stdout,argv[0]);
            exit(EXIT_SUCCESS);
        default:
            USAGE(stderr,argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (id==0 || duration==0) {
        USAGE(stderr,argv[0]);
        exit(EXIT_FAILURE);
    }
     
    if (fifo!=NULL) {
        pid_t cpid;
        
        unlink(fifo);
        mkfifo(fifo, 0600);
                
        cpid = fork();
        if (cpid == -1) {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        
        if (cpid == 0) { // child
            fd = open(fifo, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
	        if (fd < 0) {
                fprintf(stderr, "child : open(%s) failed, %s\n", fifo, strerror(errno));
		        exit(EXIT_FAILURE);
	        }

            ret = worker('c', fd, id, duration*1000, verbose, pubsub);
		    if (ret < 0) {
                fprintf(stderr, "child : worker(%s) error(%i)\n", fifo, ret);
            }

            printf("child : bye!\n");
            close(fd);
            exit(EXIT_SUCCESS);

        } else { // parent
            fd = open(fifo, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
	        if (fd < 0) {
                fprintf(stderr, "parent: open(%s) failed, %s\n", fifo, strerror(errno));
		        exit(EXIT_FAILURE);
	        }
            
            ret = worker('p', fd, id, duration*1000, verbose, pubsub);
		    if (ret < 0) {
                fprintf(stderr, "parent: worker(%s) error(%i)\n", fifo, ret);
            }

            printf("parent: bye!\n");
            close(fd);
            exit(EXIT_SUCCESS);
        }
    }
    else {
		struct termios cfg;
        
	    fd = open(name, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
	    if (fd < 0) {
            fprintf(stderr, "serial: open(%s) failed, %s\n", name, strerror(errno));
		    exit(EXIT_FAILURE);
	    }
        
		memset(&cfg, 0, sizeof(cfg));
		cfg.c_cflag = B115200 | CS8 | CLOCAL | CREAD;    
		cfg.c_iflag = IGNPAR; // ignore frame errors
		ret = tcsetattr(fd, TCSANOW, &cfg);
		if (ret < 0) {
            fprintf(stderr, "serial: tcsetattr(%s) failed, %s", name, strerror(errno));
			exit(EXIT_FAILURE);
		}
        
        ret = worker('s', fd, id, duration*1000, verbose, pubsub);
		if (ret < 0) {
            fprintf(stderr, "serial: create worker(%s) failed", name);
			exit(EXIT_FAILURE);
        }
    }
    
    exit(EXIT_SUCCESS);
}

