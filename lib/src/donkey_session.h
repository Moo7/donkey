#ifndef DONKEY_SESSION_H
#define DONKEY_SESSION_H

#include "config.h"

#if HAVE_STDINT_H
#include <stdint.h>
#endif

#include "donkey.h"

typedef bool (*donkey_f_publish)(void* owner, donkey_type_t type, uint16_t id);
typedef void (*donkey_f_consume)(void* owner, donkey_type_t type, uint16_t id);

/**
 * Linked list element used for storing memory maps
 */
typedef struct donkey_ll {
	uint16_t			id;
	void*				mmap;
	struct donkey_ll*	next;
} donkey_ll_t;

void* donkey_session_create(size_t size_100ms, size_t size_1s);
int donkey_session_destroy(void* session);
void donkey_session_reset(void* session);

donkey_ll_t* donkey_session_sub(void* session, donkey_type_t type);
donkey_ll_t* donkey_session_pub(void* session, donkey_type_t type);

void* donkey_session_publication(void* session, uint16_t id, size_t size, donkey_type_t type);
size_t donkey_session_pub_avail(void* session, donkey_type_t type);
void* donkey_session_subscription(void* session, uint16_t id, size_t size, donkey_type_t type);
size_t donkey_session_sub_avail(void* session, donkey_type_t type);

donkey_ll_t* donkey_session_pub_get(void* session, donkey_f_publish publish, void* owner);
void donkey_session_pub_synced(void* session);

uint8_t* donkey_session_add_info(void* session, uint8_t* p);
uint8_t* donkey_session_add_crc(void* session, uint8_t* p);
bool donkey_session_info_is_valid(void* session, uint8_t* p);
bool donkey_session_crc_is_valid(void* session, uint8_t* p);

#endif // DONKEY_SESSION_H

