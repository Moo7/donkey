#ifndef DONKEY_LINK_H
#define DONKEY_LINK_H

#include "config.h"

#if HAVE_STDINT_H 
#include <stdint.h>
#endif

#if HAVE_STDBOOL_H 
#include <stdbool.h>
#endif

#include "donkey_frame.h"
#include "donkey_session.h"

// size in bytes of data fields
#define DONKEY_D_ID				2
#define DONKEY_D_FR				2
#define DONKEY_D_LEN			2
#define DONKEY_D_LENX			2
#define DONKEY_D_SEG			4

// frame data (when using serial link of 115K2)
#define DONKEY_DATA_SIZE		1106

// size and offsets of '100ms' data section
#define DONKEY_D_100MS			256
#define DONKEY_S_100MS			(DONKEY_D_ID + DONKEY_D_LEN + DONKEY_D_100MS)
#define DONKEY_O_100MS			(DONKEY_O_DATA)
#define DONKEY_O_100MS_ID		(DONKEY_O_100MS)
#define DONKEY_O_100MS_LEN		(DONKEY_O_100MS_ID + DONKEY_D_LEN)
#define DONKEY_O_100MS_DATA		(DONKEY_O_100MS_LEN + DONKEY_D_LEN)

// size and offsets of '1s' data section
#define DONKEY_D_1S				260
#define DONKEY_S_1S				(DONKEY_D_FR + DONKEY_D_1S)
#define DONKEY_O_1S				(DONKEY_O_100MS + DONKEY_S_100MS)
#define DONKEY_O_1S_FR			(DONKEY_O_1S)
#define DONKEY_O_1S_DATA		(DONKEY_O_1S + DONKEY_D_FR)

// size of memory maps
#define DONKEY_MMAP_100MS		(DONKEY_S_100MS)
#define DONKEY_MMAP_1S			(DONKEY_D_1S * 10)

// size and offsets of 'serialized/mmap' data section
#define DONKEY_D_SER			584
#define DONKEY_S_SER			(DONKEY_D_SER)

// data size of pub/sub size and number of elements
#define DONKEY_D_SIZE			2
#define DONKEY_D_LSIZE			4
#define DONKEY_D_NREL			2
#define DONKEY_D_CRC			2
#define DONKEY_D_SESSION_INFO	((DONKEY_D_SIZE + DONKEY_D_NREL) * 4 + (DONKEY_D_LSIZE + DONKEY_D_NREL) * 2)
#define DONKEY_D_SESSION_CRC	((DONKEY_D_CRC + DONKEY_D_CRC) * 2 * 3)
#define DONKEY_D_SESSION		(DONKEY_D_SESSION_INFO + DONKEY_D_SESSION_CRC)

// frame bits and bitmasks
#define DONKEY_B_BLOCK			0x8000 // not used, reserved
#define DONKEY_M_ID				0x7FFF
#define DONKEY_B_SEG			0x8000
#define DONKEY_B_LAST			0x4000
#define DONKEY_M_LEN			0x3FFF

void* donkey_link_create(uint16_t id, donkey_dev_t device, void* session,
	donkey_f_publish publish, donkey_f_consume consume, void* owner);

int donkey_link_destroy(void* link);
int donkey_link(void* link, uint32_t t_elapsed);

donkey_state_t donkey_link_get_state(void* link);
bool donkey_link_is_connected(void* link);
bool donkey_link_is_online(void* link);

uint32_t donkey_link_get_timeout(void* link);
void donkey_link_set_timeout(void* link, uint32_t timeout);
void donkey_link_reset(void* link);

donkey_info_t donkey_link_pub_info(void* link, uint16_t id, size_t size, donkey_type_t type);
donkey_info_t donkey_link_sub_info(void* link, uint16_t id, size_t size, donkey_type_t type);

#endif // DONKEY_LINK_H

